package mytodo.task

abstract class TaskList(val goal : Goal){
  private var _contents : List[Long] = List();
  def contents = _contents;
  def contents_=(l : List[Long]) = _contents = l;
  def add(t : Task) = {
    if( !contents.exists(_ == t) ){
       _contents = (t.getID)::_contents;
    }
  }

  private var _name : String = "";
  def name = _name;
  def name_=(n : String) = _name = n;
  
  def getID : Long;
  
  override def toString = name;
}