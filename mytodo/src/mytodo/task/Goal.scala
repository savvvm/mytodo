package mytodo.task

abstract class Goal(var name : String){

  def getID : Long;
  def ==(r : Goal) = r != null && r.getID == getID;
  override def equals(r : Any) = r != null && r.isInstanceOf[Goal] && r.asInstanceOf[Goal].getID == getID;

  private var _parent : Option[Goal] = None
  def parent = _parent;
  def parent_=(p : Goal) = {
    if(p != null && (parent.isEmpty || parent.get.getID != p.getID)){
      _parent = Some(p);
      p.addChild(this);
    }
  }
  
  private var _children : List[Goal] = List();
  def children = _children;
  def addChild(goal : Goal) : Unit = {
    if( (_children filter (_.getID == goal.getID)).length <= 0){
      _children = goal::_children;
      goal.parent = this;
    }
  }

  override def toString = name;

}