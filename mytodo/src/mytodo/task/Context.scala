package mytodo.task

abstract class Context(var name : String){

  def getID : Long;
  override def toString = name;
}