package mytodo.task.reminder

import java.util.TimerTask
import java.util.Timer
import javafx.application.Platform
import java.util.Calendar

object AlarmManager{

  def getRepeatFunction(id : Int) : RepeatFunction = {
    return null;
  }
  
  var reminders : List[Reminder] = List();
  
  private def schedule(r : Reminder) = {
    val timer : Timer = new Timer();
    timer.schedule(new RemindTask(r), r.time.getTime);
  }

  def registerReminder(r : Reminder) = {
    if(!reminders.contains(r)){
      schedule(r);
      reminders = r :: reminders;
    }
  }
  
  def delay(r : Reminder) = {
    assert(reminders.contains(r));
    val newTime = Calendar.getInstance();
    newTime.add(Calendar.MINUTE, r.repeatMins);
    r.time = newTime;
    schedule(r);
  }
  
  private class RemindTask(r : Reminder) extends TimerTask{
    def run = Platform.runLater(r);
  }
}

object RemindMode extends Enumeration{
  type RemindMode = Value;
  val Message = Value("Show message");
}


