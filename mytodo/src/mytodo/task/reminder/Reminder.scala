package mytodo.task.reminder

import java.util.Calendar
import mytodo.ui.windows.reminder.MessageRemindWindow

abstract class Reminder(var time : Calendar, val repeatMins: Int) extends Runnable{
  def getID : Int = 0; //TODO: NotImplemented
}

class MessageReminder(time : Calendar, var msg : String) extends Reminder(time, 5){
  def run() = {
    val window = new MessageRemindWindow(msg)
    window.showAndWait();
    if(window.delayNeeded){
      AlarmManager.delay(this);
    }
  }
}