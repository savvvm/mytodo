package mytodo.task.reminder

import java.util.Calendar
import java.util.Calendar._
import mytodo.Util

abstract class RepeatFunction{
  def repeat( args : (Option[Calendar], Option[Calendar]) ) : (Option[Calendar], Option[Calendar]) = args match{
    case (None, None)        => (None, None);
    case (None, Some(c))     => ( None, Some(repeatOnlyEnd(c)) );
    case (Some(c), None)     => (Some(repeatOnlyStart(c)), None);
    case (Some(s), Some(e))  => {
      val bouth = repeatBouth(s, e);
      return (Some(bouth._1), Some(bouth._2));
    }
  }
  def getID : Long;
  protected def diff(args : (Calendar, Calendar)) = args._2.getTimeInMillis() - args._1.getTimeInMillis();
  protected def repeatOnlyEnd(end : Calendar) : Calendar;
  protected def repeatOnlyStart(start : Calendar) : Calendar;
  protected def repeatBouth(start : Calendar, end : Calendar) : (Calendar, Calendar);
  override def toString : String;
}

 object RepeatMethod extends Enumeration{
   type RepeatMethod = Value;
   val NotRepeat           = Value("Do not repeat task");
   val RepeatAtDate        = Value("Repeat at certain date");
   val DayOfMonth          = Value("Repeat at specified day every month");
   val RepeatAfterInterval = Value("Repeat after an interval");
 }

object NotRepeat extends RepeatFunction{
  //XXX: probably, this object should be removed and replaced with None
  override def repeat( args : (Option[Calendar], Option[Calendar]) ) : (Option[Calendar], Option[Calendar]) = (None, None);
  override def repeatOnlyEnd(end : Calendar) : Calendar = throw new UnsupportedOperationException;
  override def repeatOnlyStart(start : Calendar) : Calendar = throw new UnsupportedOperationException;
  override def repeatBouth(start : Calendar, end : Calendar) : (Calendar, Calendar) = throw new UnsupportedOperationException;
  override def getID : Long = 0;
  override def toString : String =  "Do not repeat";
}

//XXX: make guard for arguments of this classes
abstract class RepeatAtdate(time : Calendar) extends RepeatFunction{
  override def repeatOnlyEnd(end : Calendar) : Calendar = time;
  override def repeatOnlyStart(start: Calendar) : Calendar = time;
  override def repeatBouth(start : Calendar, end : Calendar) : (Calendar, Calendar) = {
    val cal = Calendar.getInstance();
    cal.setTimeInMillis( time.getTimeInMillis() + diff((start, end)));
    return (time, cal);
  }
  override def toString  = "Repeat once at " + Util.DateFormats.fullFormatter.format(time.getTime());
}

abstract class DayOfMonth(dayNumber : Int) extends RepeatFunction{
  private def repeat(cal : Calendar) : Calendar = {
    val next = cal.clone.asInstanceOf[Calendar];
    next.add(Calendar.MONTH, 1);
    /*
    checks if there is less days, than declared in repeater
    and if it is, sets function to last day of month.
    in next month, checks again and sets day number as it was declared
    */
    if(dayNumber > next.getActualMaximum(DAY_OF_MONTH)){
      next.set(DAY_OF_MONTH, next.getActualMaximum(DAY_OF_MONTH))
    }
    else{
      if(dayNumber > next.get(DAY_OF_MONTH)){
        next.set(DAY_OF_MONTH, dayNumber);
      }
    }
    return next;
  }
  def repeatOnlyEnd(end : Calendar) : Calendar = repeat(end);
  def repeatOnlyStart(start : Calendar) : Calendar = repeat(start);
  def repeatBouth(start : Calendar, end : Calendar) : (Calendar, Calendar) = {
      val nextStart = repeat(start);
      val nextEnd = Calendar.getInstance();
      nextEnd.setTimeInMillis(nextStart.getTimeInMillis() + diff(start, end));
      return (nextStart, nextEnd);
    }
  override def toString =  "Repeat every month at " + dayNumber +"th";
}

abstract class RepeatAfterInterval(millis : Long) extends RepeatFunction{
  def next(c : Calendar) : Calendar = {
    val next = c.clone.asInstanceOf[Calendar];
    next.setTimeInMillis(millis + c.getTimeInMillis());
    return next;
  }
  def repeatOnlyEnd(end : Calendar) : Calendar = next(end);
  def repeatOnlyStart(start : Calendar) : Calendar = next(start);
  def repeatBouth(start : Calendar, end : Calendar) : (Calendar, Calendar) = {
    val newStart = next(start);
    val newEnd = Calendar.getInstance();
    newEnd.setTimeInMillis( newStart.getTimeInMillis() + diff(start, end) );
    return (newStart, newEnd);
  }

  override def toString : String = { //TESTME
    import java.util.concurrent.TimeUnit._;
    val hours   = MILLISECONDS.toHours(millis);
    val minutes = MILLISECONDS.toMinutes(millis - HOURS.toMillis(hours));
    return "Repeat after %02d hours and %02d minutes".format(hours, minutes);
  }
  
}