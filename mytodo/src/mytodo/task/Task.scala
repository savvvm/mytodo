package mytodo.task

import java.util.Calendar
import mytodo.task.reminder.RepeatFunction
import mytodo.task.reminder.Reminder
import java.text.SimpleDateFormat
import mytodo.task.reminder.NotRepeat
import java.lang.reflect.Field

abstract class Task(var name: String){

  private var _startTime : Option[Calendar] = None;
  def startTime = _startTime;
  def startTime_=(cal : Calendar) = _startTime = Option(cal);
  def startTimeInMills : Option[Long] = _startTime match{
    case None => None;
    case Some(c) => Some(c.getTimeInMillis());
  }
  
  private var _endTime : Option[Calendar] = None;
  def endTime = _endTime;
  def endTime_=(cal : Calendar) = _endTime = Option(cal);
  def endTimeInMills : Option[Long] = _endTime match{
    case None => None;
    case Some(c) => Some(c.getTimeInMillis());
  }
  
  private var _description : String           = "";
  def description : String = { assert(_description != null);_description; }
  def description_=(s : String) = if(s == null) _description = ""; else _description = s;

  private var _repeat : RepeatFunction   = NotRepeat;
  def repeat = _repeat;
  def repeat_=(r : RepeatFunction) = _repeat = r;

  private var _remindMethod : Reminder         = null;
  def remindMethod = _remindMethod;
  def remindMethod_=(r : Reminder) = _remindMethod = r;

  private var _doneState : Boolean          = false;
  def isDone    = _doneState;
  def setDone   = _doneState = true;
  def setUndone = _doneState = false;
  def setDoneState(s : Boolean) = _doneState = s;

  private var _parents : List[Long]   = List();
  def parents = _parents;
  def addParent(parent : TaskList) = {
    parent.add(this);
    _parents = _parents ++ List(parent.getID);
  }
  def remParent(parent : TaskList) = _parents = _parents filter (_ != parent);

  def next(parent : TaskList) : Option[Long] = {
    assert(_parents.contains(parent));
    for(i <- 0 to parent.contents.length - 1){
      if(parent.contents(i) == this && i+1 < parent.contents.length){
        return Some(parent.contents(i+1));
      }
    }
    return None;
  }

  def previous(parent : TaskList) : Option[Long] = {
    assert(_parents.contains(parent));
    for(i <- 0 to parent.contents.length - 1){
      if(parent.contents(i) == this && i > 0){
        return Some(parent.contents(i-1));
      }
    }
    return None;
  }

  private var _contexts : List[Context] = List();
  def contexts = _contexts;
  def addContext(c : Context) = _contexts = _contexts ++ List(c);
  def remContext(c : Context) = _contexts = _contexts filter (_ != c);
  
  
  def getID : Long;


  def debugImage : String =  {

    def getFields[C](c : Class[C]) : Array[Field] = {
      if(c == classOf[Task].getSuperclass()){
    	  return Array();
      }
      else{
        val fields : Array[Field] = c.getDeclaredFields;
        return fields ++ getFields(c.getSuperclass());
      }
    }

    val fields = getFields( getClass );
    val methods = fields flatMap { field =>
      field.setAccessible(true);
      field.getName() + " ==> \"" + String.valueOf(field.get(this)) + "\"\n"
    }
    return String.valueOf(methods);
  }
  
  def toString(o : Option[Calendar]) = {
    o match{
      case None       => "None";
      case Some(date) => new SimpleDateFormat("YYYY.MM.dd hh:mm:ss").format(date.getTime())
    }
  }

  override def toString = name;
  override def equals(t : Any) = t.isInstanceOf[Task] && getID == t.asInstanceOf[Task].getID;

}