package mytodo;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import mytodo.datasource.DataSource;
import mytodo.datasource_sql.SqlDataSource;
import mytodo.ui.windows.main_window.MainWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main extends Application{
	
	public static DataSource dataSource = new SqlDataSource();
	
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        if(!Configuration.exists()){
            Configuration.initializeHome();
        }
        dataSource.open(Configuration.dbPath());
        Platform.setImplicitExit(false);
    	MainWindow mainWindow = new MainWindow(primaryStage);
    	mainWindow.open();
        awtTrayWorkaround(primaryStage); //XXX: remove in JavaFX8 version
    }

    private void awtTrayWorkaround(Stage primaryStage) {
        if(SystemTray.isSupported()){
            SystemTray tray = SystemTray.getSystemTray();
            String imagePath = getClass().getResource(Util.Paths$.MODULE$.icons_check_png()).getPath();
            //TODO: this image should be 16x16 and image path should be replaced with path of other image
            Image      image = Toolkit.getDefaultToolkit().getImage(imagePath);
            PopupMenu  menu = new PopupMenu();
            MenuItem   item = new MenuItem(Util.Strings$.MODULE$.exit());
            item.addActionListener(new ExitActionListener());
            menu.add(item);
            TrayIcon icon = new TrayIcon(image, Util.Strings$.MODULE$.tray_popup_tooltip(), menu);
            icon.addActionListener(new StageShowAction(primaryStage));
            try {
                tray.add(icon);
            } catch (AWTException e) {
                //TODO: empty catch block
                e.printStackTrace();
            }

        }
    }

    class ExitActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    class StageShowAction implements ActionListener{
        private Stage stage = null;

        private StageShowAction(Stage stage){
            this.stage = stage;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Platform.runLater( new Runnable() {
                @Override
                public void run() {
                    if(stage.isShowing()){
                        stage.hide();
                    }
                    else{
                        stage.show();
                    }
                }
            });
        }
    }

}
