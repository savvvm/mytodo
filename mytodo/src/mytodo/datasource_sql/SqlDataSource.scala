package mytodo.datasource_sql

import mytodo.datasource.DataSource
import scala.collection.immutable.List
import mytodo.NotImplemented
import mytodo.task.Task
import mytodo.task.TaskList
import mytodo.task.Goal
import mytodo.task.Context
import scala.slick.driver.H2Driver.simple._
import scala.slick.lifted.{ForeignKeyAction, DDL}
import scala.slick.jdbc.meta.MTable
import mytodo.datasource.DataError
import java.util.Calendar
import org.xml.sax.helpers.NewInstance
import mytodo.task.reminder.Reminder
import mytodo.task.reminder.Reminder
import mytodo.task.reminder.AlarmManager
import com.sun.org.omg.CORBA.ContextIdentifierHelper
import scala.slick.jdbc.meta.MTablePrivilege
import scala.slick.jdbc.StaticQuery
import mytodo.task.reminder.DayOfMonth
import mytodo.task.reminder.RepeatAfterInterval
import mytodo.task.reminder.RepeatAtdate
import mytodo.task.reminder.RepeatMethod
import mytodo.task.reminder.RepeatMethod._
import mytodo.task.reminder.RepeatFunction
import mytodo.task.reminder.NotRepeat
import mytodo.task.reminder.RepeatMethod

class SqlDataSource extends DataSource {

  private class SqlContext(name : String, id : Long) extends Context(name){
    override def getID = id;
  }

  private class SqlTask(name : String, id : Long) extends Task(name){
    override def getID = id;
  }
  
  private class SqlTaskList(goal : Goal, id : Long) extends TaskList(goal){
    override def getID = id;
  }

  private class SqlGoal(name : String, id : Long) extends Goal(name){
    override def getID = id;
  }
  
  
  private class SqlDayOfMonth(id : Long, dayNumber : Int) extends DayOfMonth(dayNumber){
    override def getID = id;
  }

  private class SqlRepeatAfterInterval(id : Long, millis : Long) extends RepeatAfterInterval(millis : Long){
    override def getID = id;
  }
  
  private class SqlRepeatAtdate(id : Long, time : Calendar) extends RepeatAtdate(time){
    override def getID = id;
  }
  
  private type TasksTableTuple = (Long,   String,      Option[Long], Option[Long],
		  						  String, Option[Long], Option[Int],  Boolean);
  
  private object Tasks extends Table[TasksTableTuple]("TASKS"){
    def id             = column[Long]("ID", O.PrimaryKey, O.AutoInc);
    def name           = column[String]("NAME");
    def startTime      = column[Option[Long]]("START_TIME");
    def endTime        = column[Option[Long]]("END_TIME");
    def descr          = column[String]("DESCRIPTION");
    def repeatFunID    = column[Option[Long]]("REPEAT_FUNCTION"); //TESTME: Add check for correctness
    def remindMethodID = column[Option[Int]]("REMIND_METHOD");   //TESTME: Add check for correctness
    def doneState      = column[Boolean]("DONE_STATE");
    
    def * = id ~ name ~ startTime ~ endTime ~ descr ~ repeatFunID ~ remindMethodID ~ doneState;
    def autoInc = name ~ startTime ~ endTime ~ descr ~ repeatFunID ~ remindMethodID ~ doneState returning id

  }
  
  private object TaskLists extends Table[(Long, Long, String)]("TASK_LISTS"){
    def id = column[Long]("ID", O.PrimaryKey, O.AutoInc);
    def goal = column[Long]("GOAL");
    def name = column[String]("NAME");
    
    def goalForeinKey = foreignKey("GOAL_FK", goal, Goals)(_.id);

    def * = id ~ goal ~ name;
    def autoInc = goal ~ name returning id;
  }
  
  private object Task2List extends Table[(Long, Long)]("TASK2LIST"){
    def task_id = column[Long]("TASK_ID");
    def list_id = column[Long]("LIST_ID");
    
    def taskForeignKey = foreignKey("TASK_FK", task_id, Tasks)(_.id, onDelete = ForeignKeyAction.Cascade);
    def listForeignKey = foreignKey("LIST_FK", list_id, TaskLists)(_.id);

    def * = task_id ~ list_id;
  }
  
  private object Contexts extends Table[(Long, String)]("CONTEXTS"){
    def id = column[Long]("ID", O.PrimaryKey, O.AutoInc); //TODO: add foreign key
    def name = column[String]("NAME");
    
    def * = id ~ name;
    def autoInc = name returning id;
  }
  
  private object Task2Context extends Table[(Long, Long)]("TASK2CONTEXT"){
    def task_id = column[Long]("TASK_ID");
    def context_id = column[Long]("CONTEXT_ID");
    
    def * = task_id ~ context_id;
  }
  
  private object Goals extends Table[(Long, String, Option[Long])]("GOALS"){
    def id     = column[Long]("ID", O.PrimaryKey, O.AutoInc);
    def name   = column[String]("NAME");
    def parent = column[Option[Long]]("PARENT");
    
    def * = id ~ name ~ parent
    def autoInc = name ~ parent returning id;
  }
  
  private object Inbox extends Table[(Long, String)]("INBOX"){
    def id    = column[Long]("ID", O.PrimaryKey, O.AutoInc);
    def value = column[String]("VALUE");
    
    def * = id ~ value;
    def autoInc = value returning id;
  }
  
  private object Repeaters extends Table[(Long, Long, Int, Option[Long], Option[Int], Option[Long])]("REPEATERS"){
    def id      = column[Long]("ID", O.PrimaryKey, O.AutoInc);
    def task_id = column[Long]("TASK_ID");
    def repType = column[Int]("REP_TYPE");
    def date    = column[Option[Long]]("DATE");
    def day     = column[Option[Int]]("DAY");
    def duration = column[Option[Long]]("DURATION");
    
    def * = id ~ task_id ~ repType ~ date ~ day ~ duration;
    def autoInc = task_id ~ repType ~ date ~ day ~ duration returning id;
    
  }

  private def checkedDBAccess[Type](function : Session => Type) : Type = {
    if(db == null){
      throw new DataError("Databese is null");
    }
    else{
      return db.withSession(function);
    }
  }

  override def createTask(name : String) : Task = {
    checkedDBAccess{ implicit s : Session =>
      val id = Tasks.autoInc.insert(name, None, None, "", None, None, false);
      return new SqlTask(name, id);
    }
  }

  private def getTask(values : TasksTableTuple)(implicit s : Session) : Task = {
    def millsToCalendar(opt : Option[Long]) : Calendar ={
      opt match{
      	case None => null
        case Some(mills) => val c = Calendar.getInstance(); c.setTimeInMillis(mills); c;
      }
    }
    val (id, name, startTime, endTime, description, repeaterFuncID, _, doneState) = values;
    val t = new SqlTask(name, id);
    t.startTime = millsToCalendar(startTime);
    t.endTime = millsToCalendar(endTime);
    t.description = description;
    t.repeat = getRepeater( repeaterFuncID.getOrElse(0.asInstanceOf[Long]) );
    t.setDoneState(doneState);

    val listQuery = for{ parents <- Task2List if parents.task_id === id } yield parents.list_id;
    listQuery foreach { parent => t.addParent(getTaskList(parent)(s)); }

    val contextQuery = for{ contexts <- Task2Context if contexts.task_id === id } yield contexts.context_id;
    contextQuery foreach { context => t.addContext(getContext(context)(s)) }

    return t;
  }


  def deleteTask(task: Task) = {
    checkedDBAccess{ implicit session: Session =>
      val list2taskQ = for{q <- Task2List if q.task_id === task.getID} yield(q);
      val taskQ      = for{q <- Tasks     if q.id === task.getID} yield(q);
      list2taskQ.delete; //XXX: replace with foreign key action
      taskQ.delete;
    }
  }

  override def getTasks : List[Task] = {
    checkedDBAccess{ implicit s : Session =>
      val query = Query(Tasks);
      return query.mapResult(getTask).list;
    }
  }

  override def saveTask(t : Task) : Long = {
    checkedDBAccess{ implicit s : Session =>
      t.parents.map  { parentid => Task2List.insert(t.getID, parentid); };
      t.contexts.map { context  => Task2Context.insert(t.getID, context.getID) };
      val query = for{q <- Tasks if q.id === t.getID} yield q;
      query.update(t.getID,
    		  t.name,
    		  t.startTimeInMills,
    		  t.endTimeInMills,
    		  t.description,
    		  Some(t.repeat.getID), //XXX: make non-nullable
    		  if(t.remindMethod == null) None; else Some(t.remindMethod.getID),
    		  t.isDone);
      return t.getID;
    }
  }

  def getTask(id : Long) : Task = {
    checkedDBAccess{ s : Session =>
      val tuple = (for{ task <- Tasks if task.id === id } yield task).first()(s);
      getTask(tuple)(s);
      
    }
  }

  override def createNotRepeat(taskID : Long) : RepeatFunction = {
    checkedDBAccess{ implicit s : Session =>
      if( Query(Tasks).filter(_.id === taskID).exists.run ){
    	  return NotRepeat;
      }
      else{
        throw new DataError("task with id " + taskID + " does not extst");
      }
    }
  }

  override def createRepeatAtDate(taskID : Long, time : Calendar) : RepeatFunction = {
    if(time before Calendar.getInstance){
      throw new DataError("Date should be in future");
    }
    else{
      checkedDBAccess{ implicit s : Session =>
        val id = Repeaters.autoInc.insert(taskID, RepeatMethod.RepeatAtDate.id, Some(time.getTimeInMillis()), None, None);
        return new SqlRepeatAtdate(id, time);
      }
    }
  }

  override def createDayOfMonth(taskID : Long, day : Int) : RepeatFunction = {
    if(day < 1 || day > 31){
      throw new DataError("Wrong day number : " + day);
    }
    else{
      checkedDBAccess{ implicit s : Session =>
        val id = Repeaters.autoInc.insert(taskID, RepeatMethod.DayOfMonth.id, None, Some(day), None);
        return new SqlDayOfMonth(id, day);
      }
    }
  }

  override def createRepeatAfterInterval(taskID : Long, interval : Long) : RepeatFunction = {
    if(interval < 0){
      throw new DataError("Interval is negative : " + interval);
    }
    else{
      checkedDBAccess{ implicit s : Session =>
        val id = Repeaters.autoInc.insert(taskID, RepeatMethod.DayOfMonth.id, None, None, Some(interval));
        return new SqlRepeatAfterInterval(id, interval);
      }
    }
  }

  private def getValue(id : Long, repType : Int)(implicit s : Session) : RepeatFunction = {
    RepeatMethod.apply(repType) match{
      case RepeatMethod.NotRepeat           => NotRepeat;
      case RepeatMethod.RepeatAtDate        => {
        val subquery = for{q <- Repeaters if q.id === id } yield q.date;
        val cal = Calendar.getInstance;
        cal.setTimeInMillis(subquery.list.head.get);
        return new SqlRepeatAtdate(id, cal);
      }
      case RepeatMethod.DayOfMonth          => {
        val subquery = for{q <- Repeaters if q.id === id } yield q.day
        return new SqlDayOfMonth(id, subquery.list.head.get);
      }
      case RepeatMethod.RepeatAfterInterval => {
        val subquery = for{q <- Repeaters if q.id === id } yield q.duration
        return new SqlRepeatAfterInterval(id, subquery.list.head.get);
      }
    }
  }

  override def getRepeater(id : Long) : RepeatFunction = {
    checkedDBAccess{implicit session: Session =>
      val query = for{q <- Repeaters if q.id === id} yield q.repType;
      getValue(id, query.list.head)(session);
    }
  }


    /*
     * 
   def getRepeatFunction(method : RepeatMethod, time : Calendar, day : Int, interval : Long) = {
     method match{
       case NotRepeat           => NotRepeat;
       case RepeatAtDate        => assert(time != null);     new RepeatAtdate(time);
       case DayOfMonth          => assert(day != null);      new DayOfMonth(day);
       case RepeatAfterInterval => assert(interval != null); new RepeatAfterInterval(interval);
     }
   }
   * *
   */
    
  
  override def createList(goal : Goal, name : String) : TaskList = {
    checkedDBAccess{ implicit s : Session =>
    	if(goal == null){
    	  throw new IllegalArgumentException("Attempt to create list with goal == null, list name is " + name); //TESTME;
    	}
    	else{
    	  val id = TaskLists.autoInc.insert(goal.getID, name);
    	  return getTaskList(id);
    	}
    }
  }

  private def getTaskList(id : Long)(implicit s : Session) : TaskList = {
    val query  = for(l <- TaskLists if l.id === id) yield (l.goal, l.name);
    val l = query.list map { pair : (Long, String) => 
      val goal = getGoal(pair._1)(s)
      val list = new SqlTaskList(goal, id);
      list.name = pair._2;
      val query = for{
        line <- Task2List if (line.list_id === id)
        task <- Tasks if (task.id === line.task_id)
      } yield (task.id);
      list.contents = query.list
      list;
    }
    return l.head;
  }

  override def getLists : List[TaskList] = {
    checkedDBAccess{ implicit s : Session =>
      val query = for(l <- TaskLists) yield l.id;
      return query.list map (id => getTaskList(id)(s))
    }
  }

  override def saveList(l : TaskList) : Unit = {
    checkedDBAccess{ implicit s : Session =>
      val listQuery = for(q <- TaskLists if q.id === l.getID) yield q;
      listQuery.update(l.getID, l.goal.getID, l.name);
      var childrenIds = l.contents;
      val query = for(q <- Task2List if q.list_id === l.getID) yield q.task_id;
      val savedChildIds = query.list;
      childrenIds = childrenIds filter (id => !savedChildIds.contains(id))
      childrenIds map (id => Task2List.insert(id, l.getID));
    }
  }

  override def getList(id : Long) : TaskList = {
    checkedDBAccess ( getTaskList(id)(_) )
  }

  override def createGoal(name : String) : Goal = {
    checkedDBAccess{ implicit s : Session =>
      val id = Goals.autoInc.insert(name, None);
      return new SqlGoal(name, id);
    }
  }

  private def getGoal(id : Long)(implicit s : Session) : Goal = { //TESTME: test getting goal's children
    val query = for(q <- Goals if q.id === id) yield (q.id, q.name);
    val data = query.list.head;
    val result = new SqlGoal(data._2, data._1);
    val children = (for {q <- Goals if q.parent === id} yield q.id).list;
    if(!children.isEmpty){
      children map { id => result.addChild(getGoal(id)) }
    }
    return result;
  }

  override def getGoals : List[Goal] = {
    checkedDBAccess{ implicit s : Session => 
      val query = for(q <- Goals if q.parent.isNull) yield q.id;
      return query.list map (x => getGoal(x));
    }
  }

  override def saveGoal(g : Goal) : Unit = {
    checkedDBAccess{ implicit s : Session =>
      val query = for(q <- Goals if q.id === g.getID) yield q;
      query.update( (g.getID, g.name, if(g.parent.isEmpty) None; else Some(g.parent.get.getID)) ); //XXX: refactor
    }
  }
  
  override def createContext(name : String) : Context = {
    checkedDBAccess{ implicit s : Session =>
      val id = Contexts.autoInc.insert(name);
      return new SqlContext(name, id);
    }
  }

  private def getContext(id : Long)(implicit s : Session) : Context = {
    val query = for(q <- Contexts if q.id === id) yield q;
    val contexts = query.list map (pair => new SqlContext(pair._2, pair._1));
    assert(contexts.length == 1);
    return contexts.head;
  }

  override def getContexts : List[Context] = {
    checkedDBAccess{ implicit s : Session =>
      val query = for(c <- Contexts) yield c.id;
      return query.list.map( id => getContext(id));
    }
  }

  override def saveContext(c : Context) = {
    checkedDBAccess{ implicit s : Session =>
      val query = for(c <- Contexts) yield c.name;
      query.update(c.name);
    }
  }

  override def putIntoInbox(value : String) : Long = { //TESTME
    checkedDBAccess{ implicit s : Session =>
      return Inbox.autoInc.insert(value);
    }
  }

  def getFromInbox(entryID : Long) : String = { //TESTME
    checkedDBAccess{ implicit s : Session =>
      val query = for(i <- Inbox if i.id === entryID) yield i.value;
      query.list.head;
    }
  }

  def getInbox : List[String] = { //TESTME
    checkedDBAccess{ implicit s : Session =>
      val query = for(i <- Inbox) yield i.value;
      query.list;
    }
  }
  
  override def isWritable : Boolean = isOpened;
  override def isOpened: Boolean = db != null;

  var db : Database = null;
  

  def getTablesList(implicit s : Session) = {
      var tables : List[String] = List();
      StaticQuery.queryNA[String]("select table_name from information_schema.tables") foreach {
        result => tables = tables ++ List(result);
      }
      tables;
  }
  
  def createIfNotExist(tableName : String, ddl : DDL)(implicit s : Session) = { //XXX: remove parameter duplication
    if(!getTablesList.exists(_ == tableName)){
      ddl.create;
    }
  }

  override def open(filename : String) = {
    val location = if(filename == null) "mem:test1;DB_CLOSE_DELAY=-1"; else "file:" + filename;
    System.out.println("Database location => " + location);
    db = Database.forURL("jdbc:h2:" + location, driver = "org.h2.Driver");
    db withSession{ implicit s: Session =>
      createIfNotExist(Tasks.tableName,        Tasks.ddl);
      createIfNotExist(Goals.tableName,        Goals.ddl);
      createIfNotExist(TaskLists.tableName,    TaskLists.ddl);
      createIfNotExist(Contexts.tableName,     Contexts.ddl);
      createIfNotExist(Task2List.tableName,    Task2List.ddl);
      createIfNotExist(Task2Context.tableName, Task2Context.ddl);
      createIfNotExist(Inbox.tableName,        Inbox.ddl);
      createIfNotExist(Repeaters.tableName,    Repeaters.ddl);
      db.withTransaction{  //Move somewhere
        if( ! Query(Repeaters).filter(_.id === 0.asInstanceOf[Long]).exists.run ){
          Repeaters.insert(0, -1, 0, None, None, None); //XXX: move somewhere
        }
      }
    }
  }

  override def close =  db = null;

  override def erase = {
    if(db == null)
      throw new DataError("Database is null, so it cannot be erased");
    else
      db withSession{ implicit s : Session =>
        (Tasks.ddl ++ TaskLists.ddl ++ Task2List.ddl ++ Contexts.ddl ++ Task2Context.ddl ++ Goals.ddl).drop;
        db = null;
      }
  }
}