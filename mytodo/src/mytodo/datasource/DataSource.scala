package mytodo.datasource

import mytodo.task.Task
import mytodo.task.TaskList
import mytodo.task.Goal
import mytodo.task.Context
import mytodo.task.reminder.RepeatFunction
import java.util.Calendar

trait DataSource {



  def createTask(name : String) : Task;
  def getTasks : List[Task];
  def saveTask(t : Task) : Long;
  def getTask(id : Long) : Task;
  def deleteTask(task: Task);

  def createNotRepeat(taskid : Long) : RepeatFunction;
  def createRepeatAtDate(taskid : Long, time : Calendar) : RepeatFunction;
  def createDayOfMonth(taskid : Long, day : Int) : RepeatFunction;
  def createRepeatAfterInterval(taskid : Long, interval : Long) : RepeatFunction;
  def getRepeater(id : Long) : RepeatFunction;
  
  def createList(goal : Goal, name : String) : TaskList;
  def getLists : List[TaskList];
  def saveList(l : TaskList) : Unit;
  def getList(id : Long) : TaskList;

  def createGoal(name : String) : Goal;
  def getGoals : List[Goal];
  def saveGoal(g : Goal) : Unit;
  
  def createContext(name : String) : Context;
  def getContexts : List[Context];
  def saveContext(c : Context);
  
  def putIntoInbox(value : String) : Long;
  def getFromInbox(entyID : Long) : String;
  def getInbox : List[String];
  
  def isWritable : Boolean;
  def isOpened   : Boolean;

  def open(uri : String);
  def close;
  def erase;

}