package mytodo

import javafx.collections.ObservableList
import javafx.collections.FXCollections
import java.text.SimpleDateFormat

object Util {

  object Paths{
    val icons_check_png    : String = "/res/icons/check.png";
    val icons_compose_png  : String = "/res/icons/compose.png";
    val icons_x_png        : String = "/res/icons/x.png";
  }

  object Strings{
    val exit               : String = "Exit";
    val tray_popup_tooltip : String = "Right click for menu, double click for restore window";
  }


  def createObservableList[T](from : List[T]): ObservableList[T] = {
    val items = FXCollections.observableArrayList[T]();
    from foreach (items.add _)
    return items;
  }

  object DateFormats{
    val fullFormatter  = new SimpleDateFormat("dd MMMM yyyy hh:mm (z)");
    val todayFormatter = new SimpleDateFormat("'Today' hh:mm (z)");
  }
}