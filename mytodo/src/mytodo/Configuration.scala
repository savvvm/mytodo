package mytodo

import java.io.File

object Configuration {

  private def userHome = System.getenv("LOCALAPPDATA");
  def homePath = userHome + "\\mytodo"
  def dbPath = homePath + "\\tasks.db"

  private val homeFiles = List(dbPath);

  private def createHomeFiles = {
    homeFiles map {s => 
      val f = new File(s);
      if(!f.exists){
        f.createNewFile();
      }
    }
  }

  def initializeHome : Unit = {
    val f = new File(homePath);
    if(!f.exists()){
      f.mkdirs();
    }
    createHomeFiles;
  }
  
  def exists : Boolean = {
    val f = new File(homePath);
    return f.exists();
  }
}