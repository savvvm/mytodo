package mytodo.test

import org.scalatest.FlatSpec
import mytodo.datasource_sql.SqlDataSource
import org.scalatest.BeforeAndAfter
import mytodo.datasource.DataError
import org.scalatest.Assertions._
import mytodo.task.reminder.NotRepeat
import mytodo.task.reminder.DayOfMonth
import mytodo.task.reminder.RepeatAfterInterval
import mytodo.task.reminder.RepeatAtdate
import java.util.Calendar

class SQLDataSourceTest extends FlatSpec with BeforeAndAfter{

  val sqlds = new SqlDataSource;
  before{
    sqlds.open(null);
  }
  
  after{
    if(sqlds.isOpened){
    	sqlds.erase;
    	sqlds.close;
    }
  }

  behavior of "Unitilialized data source"

  it should "not allow creating tasks" in {
	sqlds.close;
    intercept[DataError]{
      sqlds.createTask("Task")
    }
  }

  it should "showld not allow getting task lists" in {
	sqlds.close;
    intercept[DataError]{
      sqlds.getTasks;
    }
  }
  
  it should "not allow saving tasks" in {
    val t = sqlds.createTask("Task");
    t.description = "description";
    sqlds.close;
    intercept[DataError]{
      sqlds.saveTask(t);
    }
  }

  it should "showld not allow creating lists" in {
	sqlds.close;
    intercept[DataError]{
      sqlds.createList(null, "List")
    }
  }
  
  it should "not allow reading lists" in{
    sqlds.close;
    intercept[DataError]{
      sqlds.getLists;
    }
  }
  
  it should "not allow saving lists" in{
    val g = sqlds.createGoal("Goal1");
    val l = sqlds.createList(g, "Task");
    sqlds.close;
    intercept[DataError]{
      sqlds.saveList(l);
    }
  }
  
  it should "not create goals" in{
    sqlds.close;
    intercept[DataError]{
      sqlds.createGoal("Goal");
    }
  }

  it should "not allow reading goals" in{
    sqlds.close;
    intercept[DataError]{
      sqlds.getGoals;
    }
  }
  
  it should "not allow saving goals" in{
    val g = sqlds.createGoal("Goal");
    sqlds.close;
    intercept[DataError]{
      sqlds.saveGoal(g);
    }
  }
  
  it should "not allow creating contexts" in{
    sqlds.close;
    intercept[DataError]{
      sqlds.createContext("Context");
    }
  }

  it should "not allow reading contexts" in{
    sqlds.close;
    intercept[DataError]{
      sqlds.getContexts;
    }
  }
  
  it should "not allow saving contexts" in{
    val c = sqlds.createContext("Context");
    sqlds.close;
    intercept[DataError]{
      sqlds.saveContext(c);
    }
  }

  it should "do not return true on isWritable" in{
    sqlds.close;
    assert(!sqlds.isWritable)
  }

  it should "return false on isOpened" in{
    sqlds.close;
    assert(!sqlds.isOpened)
  }
  
  behavior of "Taks reading and writing"
  
  it should "create task with correct initial state" in{
    val t = sqlds.createTask("Task");
    assert(t.name == "Task");
    assert(t.startTime == None);
    assert(t.endTime == None);
    assert(t.description == "");
    assert(t.repeat == NotRepeat);
    assert(t.remindMethod == null);
    assert(!t.isDone);
    assert(t.parents != null && t.parents.isEmpty);
    assert(t.parents != null && t.contexts.isEmpty);
  }
  
  it should "correctly read list of tasks" in{
    val t1 = sqlds.createTask("Task1");
    val t2 = sqlds.createTask("Task2");
    val t3 = sqlds.createTask("Task3");
    val t4 = sqlds.createTask("Task1");
    assert(sqlds.getTasks.length == 4);
    assert(sqlds.getTasks.removeDuplicates.length == 4);
    assert(sqlds.getTasks.tail.length == 3);
  }
  
  it should "correctly save and restore tasks" in{
    val t1 = sqlds.createTask("Task");
    t1.description = "Description";
    sqlds.saveTask(t1);
    val t2 = sqlds.getTasks.head;
    assert(t2.description == "Description");
  }

  behavior of "tak lists reading and writing"

  it should "correctly create task lists" in {
    val g  = sqlds.createGoal("Goal");
    val tl = sqlds.createList(g, "List");
    assert(tl.goal == g);
    assert(tl.contents != null && tl.contents.isEmpty);
    assert(tl.name == "List");
  }
  
  it should "correctly read list of lists" in{
    val tl1 = sqlds.createList(sqlds.createGoal("Goal"), "List1");
    val tl2 = sqlds.createList(sqlds.createGoal("Goal"), "List2");
    val tl3 = sqlds.createList(sqlds.createGoal("Goal"), "List3");
    sqlds.saveList(tl1); sqlds.saveList(tl2); sqlds.saveList(tl3);
    assert(sqlds.getLists.length == 3);
  }

  it should "correctly save lists " in{
    val tl1 = sqlds.createList(sqlds.createGoal("Goal"), "List1");
    val tl2 = sqlds.createList(sqlds.createGoal("Goal"), "List2");
    val tl3 = sqlds.createList(sqlds.createGoal("Goal"), "List3");
    val t1 = sqlds.createTask("Task1"); tl1.add(t1);
    val t2 = sqlds.createTask("Task2"); tl2.add(t2);
    val t3 = sqlds.createTask("Task2");
    tl2.add(t3); tl3.add(t1); tl3.add(t2); tl3.add(t3); tl3.add(t1);
    sqlds.saveList(tl1); sqlds.saveList(tl2); sqlds.saveList(tl3);
    val ll = sqlds.getLists;
    assert(ll.filter(_.name == "List1").head.contents.length == 1 );
    assert(ll.filter(_.name == "List2").head.contents.length == 2 );
    assert(ll.filter(_.name == "List3").head.contents.length == 3 );
  }
  
  behavior of "goals"
    
  it should "correctly save goals" in{
    val g1 = sqlds.createGoal("Goal1");
    val g2 = sqlds.createGoal("Goal1");
    assert(g1 != g2);
    assert(g1.name == "Goal1");
  }
  
  it should "correctly read goals list" in{
    val g1 = sqlds.createGoal("Goal1");
    val g2 = sqlds.createGoal("Goal1");
    assert(sqlds.getGoals.length == 2);
  }
  
  it should "correctly save goals data" in{
    val g1 = sqlds.createGoal("Goal1");
    val g3 = sqlds.createGoal("Goal3");
    g1.name = "Goal2";
    g1.parent = g3;
    g3.children.head.getID == g1.getID;
    sqlds.saveGoal(g1);
    sqlds.saveGoal(g3);

    val g2 = sqlds.getGoals.head;
    assert(g2.getID == g2.getID);
    assert(g2.name == "Goal3");
    System.err.println(g2.children.head);
    assert(g2.children.head.parent.get.getID == g3.getID);
    
    val g5 = (sqlds.getGoals filter (_.name == "Goal3") ).head
    
    assert(g5.children.length == 1);
  }
  
  behavior of "contexts"
  
  it should "correctly create context" in{
    val c = sqlds.createContext("Context");
    assert(c.name == "Context");
  }
  
  it should "correctly read context list" in{
    val c1 = sqlds.createContext("Context");
    val c2 = sqlds.createContext("Context");
    val c3 = sqlds.createContext("Context");
    assert(sqlds.getContexts.length == 3);
  }
  
  it should "correctly save contexts" in{
    val c = sqlds.createContext("Context");
    c.name = "Context2";
    sqlds.saveContext(c);
    assert( sqlds.getContexts.head.name == "Context2");
  }
  
  behavior of "creating repeat functions"
    
  it should "create objects on correct arguments" in{
    val t1 = sqlds.createTask("Task1");
    val repeat1 = sqlds.createNotRepeat(t1.getID);
    assert(repeat1.getClass == NotRepeat.getClass)

    val t2 = sqlds.createTask("Task2");
    val repeat2 = sqlds.createDayOfMonth(t2.getID, 4);
    assert(repeat2.isInstanceOf[DayOfMonth])
    
    val t3 = sqlds.createTask("Task3");
    val repeat3 = sqlds.createRepeatAfterInterval(t3.getID, 1500);
    assert(repeat3.isInstanceOf[RepeatAfterInterval]);
    
    val t4 = sqlds.createTask("Task4");
    val repeat4 = sqlds.createRepeatAtDate(t4.getID, Calendar.getInstance);
    assert(repeat4.isInstanceOf[RepeatAtdate]);
  }
  
  it should "raise an exception on incorrect arguments" in{
    intercept[DataError]{
      sqlds.createNotRepeat(-1);
    }

    intercept[DataError]{
      val t2 = sqlds.createTask("Task2");
      val repeat2 = sqlds.createDayOfMonth(t2.getID, 66);
    }
    
    intercept[DataError]{
      val t3 = sqlds.createTask("Task3");
      val repeat3 = sqlds.createRepeatAfterInterval(t3.getID, -832);
    }
    
    intercept[DataError]{
      val t4 = sqlds.createTask("Task4");
      val cal = Calendar.getInstance();
      cal.set(2009, 11, 25, 13, 30, 20);
      val repeat4 = sqlds.createRepeatAtDate(t4.getID, cal);
    }
  }
}