package mytodo.test;

import javafx.scene.Scene;
import javafx.stage.Stage;

import mytodo.datasource.DataSource;
import mytodo.task.Goal;
import mytodo.ui.widgets.views.add_task.AddTask;

public class AddTaskTest extends WindowTest {
	
	public static void main(String[] args){
		launch(args);
	}

	@Override
	protected Stage makeWindow() {
		Stage stage = new Stage();
		AddTask uc = new AddTask(ds, stage);
		Scene scene = new Scene(uc);

		stage.setTitle("Test applicatin for window AddTask");
		stage.setScene(scene);
		return stage;
	}
	
	@Override
	protected String getWindowName(){
		return "AddTask";
	}

	@Override
	public void start(Stage stage) throws Exception {
		super.start(stage);
	}

	@Override
	protected void prepareTestDataSource(DataSource test) {
		Goal g = test.createGoal("Goal1");
		test.createList(g, "List1");
		test.createList(g, "List2");
		test.createList(g, "List3");
		test.createList(g, "List4");
		test.createContext("context1");
		test.createContext("context2");
		test.createContext("context3");
		ds = test;
	}
	
	private DataSource ds;

}
