package mytodo.test;

import java.text.SimpleDateFormat;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import mytodo.datasource.DataSource;
import mytodo.ui.widgets.restricted_text.DateTimeInput;

public class DateTimeInputTest extends WindowTest {

	@Override
	protected String getWindowName() {
		return "DateTimeInputTest";
	}

	@Override
	protected Stage makeWindow() {
		Stage stage = new Stage();
		HBox   hb  = new HBox();
		Button btn = new Button("Push me!");
		final DateTimeInput dti = new DateTimeInput();
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				System.err.println(dti.getRestrictedText());
				System.err.println(
				  new SimpleDateFormat("YYYY.MM.dd hh:mm:ss").format(dti.getTime().getTime())
				);
			}
		});
		hb.getChildren().add(dti);
		hb.getChildren().add(btn);
		Scene scene = new Scene(hb);
		stage.setScene(scene);
		return stage;
	}

	public static void main(String[] args){
		launch(args);
	}

	@Override
	protected void prepareTestDataSource(DataSource test) { }

}
