package mytodo.test;

import javafx.scene.Scene;
import javafx.stage.Stage;

import mytodo.datasource.DataSource;
import mytodo.ui.widgets.views.add_goal.AddGoal;

public class AddGoalTest extends WindowTest {

	public static void main(String[] args){
		launch(args);
	}

	@Override
	protected String getWindowName() {
		return "AddGoal";
	}

	@Override
	protected Stage makeWindow() {
		Stage stage = new Stage();
		AddGoal uc = new AddGoal(this.getDataSource());
		Scene scene = new Scene(uc);

		stage.setTitle("Test applicatin for window AddTask");
		stage.setScene(scene);
		return stage;
	}

	
	@Override
	protected void prepareTestDataSource(DataSource test) {
		test.createGoal("Goal 1");
		test.createGoal("Goal 2");
		test.createGoal("Goal 3");
	}

}
