package mytodo.test

import org.scalatest.FlatSpec
import java.util.Calendar
import java.util.Calendar._
import org.scalatest.Assertions
import org.scalatest.Assertions._
import mytodo.task.reminder.RepeatFunction
import mytodo.datasource_sql.SqlDataSource

class RepeatFunctionTest extends FlatSpec { 

  val ds = new SqlDataSource;
  ds.open(null);
  
  behavior of "RepeatAtDate"
  
  val oldStart = Calendar.getInstance();
  oldStart.set(2014, DECEMBER, 31, 13, 25, 47);

  val oldEnd = Calendar.getInstance();
  oldEnd.set(2016, NOVEMBER, 17, 21, 32, 43);

  val newStart = Calendar.getInstance();
  newStart.set(2023, JUNE, 05, 22, 33, 11);

  private def checkNones(args  : ( Option[Calendar],  Option[Calendar]) ) = {
    assert(args._1 == None);
    assert(args._2 == None);
  }
  
  def checkLeftNone(args : (Option[Calendar], Option[Calendar]), check : (Calendar => Boolean) ) = {
    assert(args._1 == None);
    assert(check(args._2.get));
  }

  def checkRightNone(args : (Option[Calendar], Option[Calendar]), check : (Calendar => Boolean) ) = {
    assert(args._2 == None);
    assert(check(args._1.get));
  }
  
  it should "set start time in date from constructor" in{ //make reusable
    val fun = ds.createRepeatAtDate(ds.createTask("").getID, newStart);
    checkNones(fun.repeat(None, None) );
    checkLeftNone(fun.repeat(None, Some(newStart)), _ == newStart);
    checkRightNone(fun.repeat(Some(newStart), None), _ == newStart);
    val (checkStart, checkEnd) = fun.repeat(Some(oldStart), Some(oldEnd));
    assert(checkStart.get == newStart);
  }
  
  def checkNewEnd[T <: RepeatFunction](fun : T) = {
    val (checkStart, checkEnd) = fun.repeat(Some(oldStart), Some(oldEnd));
    val oldDiff = oldEnd.getTimeInMillis() - oldStart.getTimeInMillis();
    val newDiff = checkEnd.get.getTimeInMillis() - checkStart.get.getTimeInMillis();
    assert( (checkStart.get) before (checkEnd.get) );
    assert( oldDiff == newDiff);
  }

  it should "new end should be after the same interval as old end" in{ //Make reusable
    val fun = ds.createRepeatAtDate(ds.createTask("").getID, newStart);
    checkNewEnd(fun);
  }
  
  behavior of "DayOfMonth"
  
  it should "set day field of calendar in certain day in next month" in{
    val fun = ds.createDayOfMonth(ds.createTask("").getID, 31);
    checkNones(fun.repeat(None, None) );
    //checkLeftNone(fun.repeat( (None, Some(newStart)) ), _ == newStart);
    //checkRightNone(fun.repeat( (Some(newStart), None) ), _ == newStart);
    val (newStart, newEnd) = fun.repeat(Some(oldStart), Some(oldEnd));
    val nexMonth : Calendar = oldStart.clone.asInstanceOf[Calendar];
    nexMonth.add(MONTH, 1);
    assert(newStart.get == nexMonth);
  }

  it should "new end should be after the same interval as old end" in{ //Make reusable
    val fun = ds.createDayOfMonth(ds.createTask("").getID, 31);
    checkNewEnd(fun);
  }
  
  it should "set day to the last day of month if there is no corresponding day number in month" in{
    val fn = ds.createDayOfMonth(ds.createTask("").getID, 31);
    val (janStart, _) = fn.repeat( (Some(oldStart), Some(oldEnd)) );
    assert(janStart.get.get(MONTH) == JANUARY);
    var (newStart, newEnd) = fn.repeat( fn.repeat( (Some(oldStart), Some(oldEnd)) ) );
    assert(newStart.get.get(MONTH) == FEBRUARY);
    assert(newStart.get.get(DAY_OF_MONTH) == 28);
    newStart = fn.repeat( (newStart, newEnd) )._1;
    assert(newStart.get.get(MONTH) == MARCH);
    assert(newStart.get.get(DAY_OF_MONTH) == 31);
  }

  behavior of "RepeatAfterInterval"

  it should "set next start after an interval in milliseconds" in{
    val fun = ds.createRepeatAfterInterval(ds.createTask("").getID, 1000);
    def nextNSec(n : Int) : Calendar = {
      val c = oldStart.clone.asInstanceOf[Calendar];
      c.add(SECOND, 1)
      return c;
    }
    val (newStart, newEnd) = fun.repeat(Some(oldStart), Some(oldEnd));
    assert(newStart.get == nextNSec(1));
    val (new3Start, new3End) = fun.repeat(fun.repeat(fun.repeat(Some(oldStart), Some(oldEnd))));
    assert(newStart.get == nextNSec(3));
  }

  it should "new end should be after the same interval as old end" in{ //Make reusable
    val fun = ds.createRepeatAfterInterval(ds.createTask("").getID, 3000);
    checkNewEnd(fun);
  }
  

}