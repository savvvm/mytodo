package mytodo.test;

import javafx.application.Application;
import javafx.stage.Stage;

import mytodo.datasource.DataSource;
import mytodo.datasource_sql.SqlDataSource;

public abstract class WindowTest extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	private String windowName;
	private SqlDataSource ds;

	protected abstract void prepareTestDataSource(DataSource test);
	protected abstract String getWindowName();
	protected abstract Stage makeWindow();

	@Override 
	public void init() {
		this.windowName = getWindowName();
	}

	
	public DataSource getDataSource(){
		return ds;
	}

	@Override
	public void start(Stage stage) throws Exception {
		ds = new SqlDataSource();
		ds.open(null);
		prepareTestDataSource(ds);
		Stage newStage = makeWindow();
		newStage.setTitle("Test applicatin for window " + windowName);
		newStage.show();
	}

}