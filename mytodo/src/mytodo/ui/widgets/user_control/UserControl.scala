package mytodo.ui.widgets.user_control

import javafx.scene.Node
import javafx.geometry.HPos
import javafx.geometry.VPos
import javafx.scene.layout.Region
import mytodo.ui.LayoutLoader
import mytodo.ui.Controller
import javafx.util.Callback

abstract class UserControl extends Region with LayoutLoader{

  def loadWithFactory(factory : Callback[Class[_], AnyRef]) = {
    getChildren.add( loadLayout(factory) );
  }

  def this(factory : Callback[Class[_], AnyRef]) = {
    this();
    loadWithFactory(factory);
  }

  @deprecated
  def this(controller : Controller) = {
    this();
    getChildren.add( loadLayout(controller) );
  }

  override protected def layoutChildren = {
	  def resizeChild(node : Node) = layoutInArea(node, 0, 0, getWidth(), getHeight(), 0, HPos.LEFT, VPos.TOP);
	  //in resizeChild type cast does not work
	  //toArray is used, because ObserbableList somewhy does not work with map
	  getChildren.toArray.map( o => resizeChild(o.asInstanceOf[Node]) );
  }
  
}

object UserControl{
}