package mytodo.ui.widgets.restricted_text

import javafx.scene.control.TextField

abstract class RestrictedText extends TextField{ //XXX: throw exception if restriction == null
  type Restriction = String => Boolean;
  var inputRestriction  : Restriction = null;
  var outputRestriction : Restriction = null;

  def setRestrictions(inputRestriction : Restriction, outputRestriction : Restriction){
    this.inputRestriction  = inputRestriction;
    this.outputRestriction = outputRestriction;
  }
  
  override def replaceText(start : Int, end : Int, text : String) = {
    if(inputRestriction != null && inputRestriction(text)){
      super.replaceText(start, end, text);
    }
  }

  override def replaceSelection(text : String) = {
    if(inputRestriction != null && inputRestriction(text)){
      super.replaceSelection(text);
    }
  }
  
  def getRestrictedText : String = {
    val text = getText();
    if(outputRestriction != null && outputRestriction(text)){
      return text;
    }
    else{
      return "";
    }
  }
}