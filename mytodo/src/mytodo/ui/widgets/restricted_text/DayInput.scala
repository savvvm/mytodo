package mytodo.ui.widgets.restricted_text

import java.text.SimpleDateFormat
import java.text.ParsePosition
import java.util.Calendar

class DayInput extends RestrictedText {
  setRestrictions(_.matches("\\d+"), dayIsCorrect(_));
  
  private def dayIsCorrect(s : String) : Boolean = {
    try{
      val int = Integer.parseInt(s)
      return int >= 1 && int <= 31;
    }
    catch{
      case e : NumberFormatException => return false;
    }
  }
  
  def getDay : Int = {
    return Integer.parseInt(getText());
  }
}