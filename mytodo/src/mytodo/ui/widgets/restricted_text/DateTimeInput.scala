package mytodo.ui.widgets.restricted_text

import java.text.SimpleDateFormat
import java.text.ParsePosition
import java.util.Calendar

class DateTimeInput extends RestrictedText {
  setRestrictions(_.matches("([\\d+:.\\s]){0,3}"), dateTimeCorrect(_));
  
  def start = new ParsePosition(0);
  val withSecs  = new SimpleDateFormat("dd.MM.yyyy kk:mm:ss");
  val withMins  = new SimpleDateFormat("dd.MM.yyyy kk:mm");
  val withHours = new SimpleDateFormat("dd.MM.yyyy kk");

  def dateTimeCorrect(s : String) : Boolean = {
    //XXX: should return false if date not exist
    return withSecs.parse(s,start) != null   ||
    	   withMins.parse(s, start)  != null ||
    	   withHours.parse(s, start) != null;
  }
  
  def getTime : Calendar = {
    val text = getRestrictedText;
    var result : Calendar = Calendar.getInstance();
    if(!text.isEmpty()){
      if( withSecs.parse(text, start) != null ){
    	  result.setTime(withSecs.parse(text, start));
      }
      else if( withMins.parse(text, start) != null ){
    	  result.setTime(withMins.parse(text, start));
      } else if( withHours.parse(text, start) != null ){
    	  result.setTime(withHours.parse(text, start));
      }
      else{
        result = null;
      }
      result;
    }
    else{
      result = null;
    }
    result;
  }
}