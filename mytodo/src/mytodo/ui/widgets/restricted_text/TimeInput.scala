package mytodo.ui.widgets.restricted_text

class TimeInput extends RestrictedText {
  setRestrictions(_.matches("(\\d+||:){0,3}"), timeIsCorrect(_));
  private def timeIsCorrect(newValue : String) : Boolean = {
    def isIntegerInRange(s : String, from : Int, to : Int) : Boolean = {
      try{
    	  val int = Integer.parseInt(s);
    	  return int >= from && int <= to;
      }
      catch{
      	case e : NumberFormatException => return false;
      }
    }
    def isHours = isIntegerInRange(_ : String, 0, 23);
    def isMins = isIntegerInRange(_ : String, 0, 59);
    def isSecs = isMins;
    val segments = newValue.split(":");
    segments.length match{
      case 1 => isHours(segments(0));
      case 2 => isHours(segments(0)) && isMins(segments(1))
      case 3 => isHours(segments(0)) && isMins(segments(1)) && isSecs(segments(2))
      case _ => false;
    }
  }
}