package mytodo.ui.widgets.task_view

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import mytodo.task.{Goal, TaskList, Task}
import mytodo.ui.Controller
import javafx.scene.control.{Button, Label}
import javafx.util.Callback
import javafx.fxml.FXML
import mytodo.{NotImplemented, Main, Util}
import java.util.Calendar
import javafx.scene.layout.{BorderPane, HBox, GridPane}
import javafx.scene.image.{Image, ImageView}
import javafx.event.{EventType, Event, EventHandler, ActionEvent}
import mytodo.ui.widgets.list_view.ListView
import mytodo.ui.widgets.goal_view.GoalView

class TaskView(t : Task, parent : BorderPane) extends UserControl(new TaskViewControllerFactory(t, parent)){
  override def bindData(n : Node) = {
    getStylesheets().add(getClass().getResource("TaskViewStyle.css").toExternalForm());
  }
}

class TaskViewControllerFactory(t : Task, parent : BorderPane) extends Callback[Class[_], AnyRef]{
  def call(p1: Class[_]): AnyRef = {
    return new TaskViewController(t, parent);
  }
}

class TaskViewController(task : Task, parent : BorderPane) extends Controller{

  private val NO_START_TIME_DEFINED : String = "No start time defined";
  private val NO_END_TIME_DEFINED   : String = "No end time defined";
  private val NO_REMINDER_DEFINED   : String = "Reminder time was not set";

  @FXML var title_output           : Label = null;
  @FXML var start_time_output      : Label = null;
  @FXML val end_time_output        : Label = null;
  @FXML var remind_date_output     : Label = null;
  @FXML var repeat_function_output : Label = null;
  @FXML var description_output     : Label = null;
  @FXML var parents_output         : GridPane = null;
  @FXML var goals_output           : GridPane = null;
  @FXML var contexts_output        : HBox     = null;

  def openNextTask(e : ActionEvent) : Unit = {
    throw new NotImplemented;
    //TODO: Not Implemented
  }

  def openPrevTask(e : ActionEvent) : Unit = {
    throw new NotImplemented;
    //TODO: Not Implemented
  }

    def initialize() = {
    title_output.setText(task.name);
    title_output.getStyleClass.add( if(task.isDone){ "done" } else{ "undone" } );

    setTimeImage(task.startTime, NO_START_TIME_DEFINED, start_time_output);
    start_time_output.getStyleClass.add("time");

    setTimeImage(task.endTime,   NO_END_TIME_DEFINED,   end_time_output);
    end_time_output.getStyleClass.add("time");

    val remtime = {
      if(task.remindMethod == null){
        None;
      }
      else{
        Some(task.remindMethod.time);
      }
    }
    setTimeImage(remtime, NO_REMINDER_DEFINED, remind_date_output);
    remind_date_output.getStyleClass.add("time");

    repeat_function_output.setText(task.repeat.toString);
    repeat_function_output.getStyleClass.add("time");

    description_output.setText(task.description);

    def makeParentButton(parent : BorderPane, l : TaskList) : Button = {
      val button : Button = makeButton[TaskList](l.name, l, "parent_button", "/res/icons/clipboard.png");
      button.addEventHandler[ActionEvent](new EventType, new EventHandler[ActionEvent]{
        def handle(e : ActionEvent) : Unit = parent.setCenter(new ListView(l));
      });
      return button;
    };
    setUpGridPane(parents_output, (task.parents map Main.dataSource.getList), 0, 0, makeParentButton(parent, _ : TaskList));

    def makeGoalButton(parent : BorderPane, g : Goal) : Button = {
      val button =  makeButton[Goal](g.name, g : Goal, "goal_button", "/res/icons/target.png");
      button.addEventHandler(new EventType, new EventHandler[ActionEvent]{
        def handle(e : ActionEvent) : Unit = parent.setCenter(new GoalView(g));
      })
      return button;
    };

    setUpGridPane(goals_output, getGoals, 0, 0, makeGoalButton(parent, _ : Goal));

    task.contexts map { ctxt => contexts_output.getChildren().add(new Label("@" + ctxt.name + " ")) }

  }

  private def setActionToButton[T](btn : Button, argument : T, handler : ActionEvent => Unit){
    btn.addEventHandler( new EventType, new h() );
    return btn;
  }

  private class h extends EventHandler[ActionEvent]{
    def handle(p1: ActionEvent): Unit = {
      return Unit;
    };
  }

  private def makeButton[T](name : String, item : T, styleClass : String, iconName : String) : Button = {
    val but = new Button(name);
    but.getStyleClass.add(styleClass); //XXX: style for setting icon
    val view = new ImageView( new Image(iconName) );
    view.setFitHeight(16.0);
    view.setFitWidth(16.0);
    but.setGraphic(view);
    return but;
  }


  private def getGoals : List[Goal] = {
    var g : List[Goal] = List();
    var parents = task.parents map Main.dataSource.getList;
    for(parent <- parents){
      if(!contains(g, parent.goal)){
        g = parent.goal::g;
      }
    }
    def contains(list : List[Goal], item : Goal) : Boolean = (list filter (_.getID == item.getID)).length > 0;
    return g;
  }

  private def setUpGridPane[T](pane : GridPane,list : List[T], column : Int, row : Int, func : T => Button) : Unit = {
    list match{
      case x::xs => {
        val but = func(x);
        pane.add(but, column, row);
        val newCol = (column + 1) % 3;
        setUpGridPane(pane, xs, newCol, if(newCol == 0) row+1; else row, func);
      }
      case List() => Unit;
    }
  }

  private def setTimeImage(time : Option[Calendar], errMsg : String, widget : Label) = {
    time match{
      case None       => widget.setText( errMsg );
      case Some(date) => widget.setText( Util.DateFormats.fullFormatter.format( date.getTime() ));
    }
  }

}