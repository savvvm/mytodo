package mytodo.ui.widgets.edit_task

import mytodo.datasource.DataSource
import javafx.stage.Stage
import javafx.scene.Node
import mytodo.ui.widgets.dialog_view.DialogView
import javafx.scene.control.CheckBox
import javafx.scene.layout.HBox
import mytodo.task.{Task, Context}
import javafx.util.Callback
import mytodo.ui.widgets.user_control.UserControl

class EditTask(task : Task, ds : DataSource, parent : Stage) extends UserControl(new ControllerFactory(ds, task, parent)){
  override protected def bindData(root : Node) = {
    val contexts_list = root.lookup("#contexts_list").asInstanceOf[HBox];
    val contexts = ds.getContexts;
    contexts map { c => contexts_list.getChildren().add(new ContextCheckBox(c))}
  };
  
}

class ControllerFactory(ds : DataSource, task : Task, parent : Stage) extends Callback[Class[_], AnyRef]{
  def call(p1: Class[_]): EditTaskController = return new EditTaskController(ds, task, parent);
}

class ContextCheckBox(c : Context) extends CheckBox{ //XXX: move this class and class from AddTask to another file
	setText( "@" + c.name);
	def getContext = c;
}
