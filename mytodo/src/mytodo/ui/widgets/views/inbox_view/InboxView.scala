package mytodo.ui.widgets.inbox_view

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import mytodo.NotImplemented
import javafx.scene.control.ListView
import mytodo.Util
import mytodo.Main
import mytodo.ui.widgets.UndefinedController

class InboxView extends UserControl(new UndefinedController){

  override protected def bindData(root : Node) = {
	val list = root.asInstanceOf[ListView[String]];
    list.setItems( Util.createObservableList(Main.dataSource.getInbox) );
  }

}