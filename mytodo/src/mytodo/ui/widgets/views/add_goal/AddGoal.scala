package mytodo.ui.widgets.add_goal

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import javafx.scene.control.ListView
import mytodo.task.Goal
import javafx.collections.FXCollections
import mytodo.datasource.DataSource
import javafx.scene.control.SelectionMode
import mytodo.ui.widgets.dialog_view.DialogView

class AddGoal(ds : DataSource) extends DialogView(new Controller(ds)){
  override def bindData(root : Node) = {
    val list : ListView[Goal] = root.lookup("#parents_list").asInstanceOf[ListView[Goal]];
    val content = FXCollections.observableArrayList[Goal]();
    ds.getGoals foreach (content.add _);
    list.setItems(content);
    list.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
  };

}