package mytodo.ui.widgets.add_goal

import javafx.fxml.FXML
import javafx.scene.control.ListView
import mytodo.task.Goal
import javafx.event.ActionEvent
import mytodo.NotImplemented
import javafx.scene.control.TextField
import mytodo.datasource.DataSource
import mytodo.ui.widgets.dialog_view.DialogViewController

class Controller(ds : DataSource) extends DialogViewController{

  @FXML var parents_list : ListView[Goal] = null;
  @FXML var name_input   : TextField = null;
  
  override def saveData : Unit = {
    val g = ds.createGoal(name_input.getText());
    g.parent = parents_list.getSelectionModel().getSelectedItems().get(0);
    ds.saveGoal(g);
  }
  
  override def cleanup(){
    name_input.setText("");
    parents_list.getSelectionModel().clearSelection();
  }
  
  override def cleanData = Unit;
  
}
  