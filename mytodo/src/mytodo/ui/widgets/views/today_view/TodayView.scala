package mytodo.ui.widgets.today_view

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import mytodo.NotImplemented
import mytodo.ui.widgets.dialog_view.DialogViewController
import javafx.scene.layout.{BorderPane, HBox, VBox}
import mytodo.datasource.DataSource
import java.util.Calendar
import java.util.Calendar._
import mytodo.task.Task
import mytodo.ui.widgets.task_widget.TaskWidget
import mytodo.ui.widgets.UndefinedController
import javafx.geometry.Insets
import javafx.stage.Stage

class TodayView(ds : DataSource, stage : Stage, parent : BorderPane) extends UserControl(new UndefinedController) {

  private var today : Calendar = null;

  override protected def bindData(root : Node) = {
    val today_tasks_box = root.lookup("#today_tasks_box").asInstanceOf[VBox];
    today = Calendar.getInstance();
    createTaskChildren foreach {child =>
      child.setPadding( new Insets(0, 0, 10, 10) );
      today_tasks_box.getChildren().add(child)
    }
  }
  
  private def createTaskChildren : List[TaskWidget] = {
    if(ds != null && ds.getTasks != null){
      return ds.getTasks.filter(isActual _).map(new TaskWidget(_, stage, parent));
    }
    else{
      List();
    }
  }

  def isActual(t : Task) : Boolean = {
    if(!t.isDone){
      t.startTime match{
        case None       => true;
        case Some(date) => (date get DAY_OF_YEAR) <= (today get DAY_OF_YEAR);
      }
    }
    else{
      false;
    }
  }
}