package mytodo.ui.widgets.task_list

import mytodo.ui.widgets.user_control.UserControl
import javafx.scene.Node
import mytodo.{NotImplemented, Main, Util}
import javafx.collections.FXCollections
import mytodo.task.Task
import javafx.scene.control.{Tooltip, ListView, ListCell, Button}
import javafx.collections.ObservableList
import javafx.util.Callback
import javafx.scene.control.cell.CheckBoxListCell
import javafx.beans.value.ObservableValue
import javafx.beans.value.ObservableBooleanValue
import javafx.beans.property.ReadOnlyBooleanProperty
import javafx.beans.property.BooleanProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.value.ChangeListener
import javafx.event.EventHandler
import javafx.event.ActionEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.BorderPane
import mytodo.ui.widgets.task_view.TaskView
import scala.swing.BorderPanel
import javafx.scene.image.{ImageView, Image}
import mytodo.ui.widgets.edit_task.EditTask
import javafx.stage.Stage

class TaskList(parentPane : BorderPane, stage : Stage) extends UserControl(new TaskListController){
 
  override def bindData(root : Node) = {
	val list = root.asInstanceOf[ListView[Task]];
    list.setCellFactory( new CellFactory(parentPane, stage, list) );
    list.setItems(Util.createObservableList(Main.dataSource.getTasks));
  }
  
  class CellFactory(parent : BorderPane, stage : Stage, list : ListView[Task]) extends Callback[ListView[Task], ListCell[Task]]{
    override def call(param : ListView[Task]) : ListCell[Task] = {
      //new OverridenCell();
      val lc = new ButtonCheckBoxCell(parent, stage, list); //CheckBoxListCell[Task];
      lc.setSelectedStateCallback( new TaskDoneSetter );
      return lc;
    }
  }
  
  class ButtonCheckBoxCell(parent : BorderPane, stage : Stage, list : ListView[Task]) extends CheckBoxListCell[Task]{
    var showButton   : Button = null;
    var editButton   : Button = null;
    var removeButton : Button = null;
    val hBox = new HBox();


    override def updateItem(t : Task, empty : Boolean) = {
      super.updateItem(t, empty);
      if(empty){
        setText(null);
        setGraphic(null);
      }
      else{
        setText(t.name);
        showButton = createButton("Show this Task", Util.Paths.icons_check_png) {e : ActionEvent =>
          parent.setCenter( new TaskView(t, parent) );
        };
        editButton = createButton("Edit this Task", Util.Paths.icons_compose_png) {e : ActionEvent =>
          parent.setCenter(new EditTask(t, Main.dataSource, stage));
        };
        removeButton = createButton("Remove this task", Util.Paths.icons_x_png) {e : ActionEvent =>
          Main.dataSource.deleteTask(t);
          forceToRefresh;
        }
        hBox.getChildren().add(super.getGraphic());
        hBox.getChildren().add(showButton);
        hBox.getChildren().add(editButton);
        hBox.getChildren().add(removeButton);
        setGraphic(hBox);
      }
    }

    private def forceToRefresh = {
      list.setItems(null);
      list.setItems(Util.createObservableList(Main.dataSource.getTasks));
    }

    private def createButton(tooltip : String, imagePath : String)( action : ActionEvent => Unit) = {
      val btn : Button = new Button();
      val tooltipObject = new Tooltip(tooltip);
      btn.setTooltip(tooltipObject);
      btn.setOnAction(new EventHandler[ActionEvent]{
        override def handle(e : ActionEvent) : Unit = {
          action(e);
        };
      });
      val image = new Image(imagePath);
      val view = new ImageView(image);
      view.setFitHeight(16.0);
      view.setFitWidth(16.0);
      btn.setGraphic(view);
      btn;
    }
  }

  class TaskShow(t : Task, parent : BorderPane) extends EventHandler[ActionEvent]{
    override def handle(e : ActionEvent){
    }
  }

  class TaskDoneSetter extends Callback[Task, ObservableValue[java.lang.Boolean]]{
    override def call(t : Task) : ObservableValue[java.lang.Boolean] = {
      val prop = new SimpleBooleanProperty(new java.lang.Boolean(t.isDone));
      prop.addListener( new TaskDoneChanger(t) );
      return prop;
    }
  }
  
  class TaskDoneChanger(t : Task) extends ChangeListener[java.lang.Boolean]{
    override def changed(observable : ObservableValue[_ <: java.lang.Boolean] , oldValue : java.lang.Boolean, newValue : java.lang.Boolean) = {
      t.setDoneState(newValue);
      Main.dataSource.saveTask(t);
    }
  }
}