package mytodo.ui.widgets.add_alarm

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import javafx.scene.control.TextField
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.fxml.FXML
import mytodo.ui.widgets.restricted_text.RestrictedText
import javafx.event.ActionEvent
import javafx.scene.control.TextArea
import mytodo.NotImplemented
import mytodo.task.reminder.MessageReminder
import java.util.Calendar
import mytodo.task.reminder.AlarmManager
import mytodo.ui.widgets.dialog_view.DialogView
import mytodo.ui.widgets.dialog_view.DialogViewController
import mytodo.ui.widgets.restricted_text.TimeInput
import javafx.scene.control.Label


class AddAlarm extends DialogView(new Controller) { //TESTME
  def bindData(root : Node) : Unit = { }

} 

private class Controller extends DialogViewController{

  val ALARM_IS_NOT_IN_FUTURE : String = "Time for alarm should be in future";

  @FXML var time_input    : TimeInput = null;
  @FXML var message_input : TextArea  = null;
  @FXML var error_output  : Label     = null;
  
  override def saveData : Unit = {
    if( !time_input.getRestrictedText.isEmpty() ){
      val calendar = Calendar.getInstance();
      val (hours, minute, seconds) = getTime(time_input.getText());
      calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
      calendar.set(Calendar.MINUTE, Integer.parseInt(minute));
      calendar.set(Calendar.SECOND, Integer.parseInt(seconds));
      if(calendar.after(Calendar.getInstance)){
        val reminder = new MessageReminder(calendar, message_input.getText());
        AlarmManager.registerReminder(reminder);
      }
      else{
        displayError(ALARM_IS_NOT_IN_FUTURE);
        canselCleanUserInput;
      }
    }
  }
  
  override def cleanup : Unit = {
    time_input.setText("");
    message_input.setText("");
    error_output.setText("");
    error_output.setVisible(false);
  }
  
  private def getTime(s : String) : (String, String, String)= {
    val segments = s.split(":");
    segments.length match{
      case 1 => (segments(0), "00", "00");
      case 2 => (segments(0), segments(1), "00")
      case 3 => (segments(0), segments(1), segments(2))
      case _ => null;
    }
  }
  
  override def cleanData = Unit;
  
  def displayError(msg : String) = {
    error_output.setText(msg);
    error_output.setVisible(true);
  }
}