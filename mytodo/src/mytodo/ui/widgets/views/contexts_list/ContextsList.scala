package mytodo.ui.widgets.contexts_list

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import mytodo.NotImplemented
import javafx.scene.control.ListView
import mytodo.Util
import mytodo.datasource.DataSource
import mytodo.task.Context
import mytodo.ui.widgets.UndefinedController

class ContextsList(ds : DataSource) extends UserControl(new UndefinedController){

  override protected def bindData(root : Node) = {
    val list = root.asInstanceOf[ListView[Context]];
    list.setItems(Util.createObservableList(ds.getContexts))
  }

}