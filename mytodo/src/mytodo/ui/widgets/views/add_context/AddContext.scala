package mytodo.ui.widgets.add_context

import mytodo.datasource.DataSource
import mytodo.ui.widgets.dialog_view.DialogView
import mytodo.ui.widgets.dialog_view.DialogViewController
import javafx.scene.Node
import mytodo.NotImplemented
import javafx.fxml.FXML
import javafx.scene.control.TextField

class AddContext(ds : DataSource) extends DialogView(new Controller(ds)){
  protected def bindData(root : Node) = Unit;

}

class Controller(ds : DataSource) extends DialogViewController{

  @FXML var context_name_input : TextField = null;

  def cleanup   =  context_name_input.setText("");
  def saveData  =  {
    val ctxt = ds.createContext(context_name_input.getText());
  }

  def cleanData =  Unit;
  
}