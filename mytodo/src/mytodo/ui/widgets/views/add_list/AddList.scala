package mytodo.ui.widgets.add_list

import mytodo.ui.widgets.dialog_view.DialogView
import javafx.scene.Node
import mytodo.ui.widgets.dialog_view.DialogViewController
import mytodo.NotImplemented
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.event.ActionEvent
import mytodo.datasource.DataSource
import javafx.scene.control.TextField
import javafx.stage.Stage
import mytodo.Main
import javafx.scene.control.ListView
import mytodo.task.Task
import javafx.beans.Observable
import javafx.collections.FXCollections
import mytodo.Util
import mytodo.task.Goal
import mytodo.task.TaskList
import javafx.scene.control.Button
import mytodo.ui.windows.dialogs.goal_dialog.GoalSelectDialog
import mytodo.ui.windows.dialogs.child_dialog.ChildSelectDialog

class AddList(ds : DataSource, parent : Stage) extends DialogView(new Controller(parent, ds)) {

  private var items = FXCollections.observableArrayList[Task]();
  def bindData(root : Node) = {
    root.lookup("#children_list").asInstanceOf[ListView[Task]].setItems(items);
    root.lookup("#save_button").asInstanceOf[Button].setDisable(true);
  }
}

class Controller(parent : Stage, ds : DataSource) extends DialogViewController{
  
  @FXML var name_input      : TextField      = null;
  @FXML var goal_name_output : Label          = null;
  @FXML var children_list   : ListView[Task] = null;
  @FXML var save_button     : Button         = null;

  var children : List[Task] = List();
  var goal     : Goal = null;
  var result   : TaskList = null;
  
  @FXML
  def openGoalSelector(event : ActionEvent) = {
    val d = new GoalSelectDialog(parent, Main.dataSource);
    d.showAndWait();
    d.result match{
      case Some(resultGoal) => {
        goal = resultGoal;
        goal_name_output.setText(goal.name);
        save_button.setDisable(false);
      }
      case _ => ;
    }
  }

  @FXML
  def openChildSelector(event : ActionEvent) = {
    val d = new ChildSelectDialog(parent, Main.dataSource);
    d.showAndWait();
    d.getResult match{
      case Some(child) => children = child::children;
      case None        => null;
    }
    children_list.setItems(Util.createObservableList(children));
  }

  def cleanup = {
    name_input.setText("");
    goal_name_output.setText("");
    children_list.setItems(FXCollections.observableArrayList());
    save_button.setDisable(true);
  }

  def saveData = {
    result = ds.createList(goal, name_input.getText());
    result.contents = children map (_.getID);
    ds.saveList(result);
  }

  def cleanData = {
    children = List();
    goal = null;
  }

}