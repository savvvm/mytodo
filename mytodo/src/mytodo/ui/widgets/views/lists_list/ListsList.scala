package mytodo.ui.widgets.lists_list

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import mytodo.NotImplemented
import mytodo.ui.widgets.dialog_view.DialogViewController
import javafx.scene.control.ListView
import mytodo.Util
import mytodo.datasource.DataSource
import mytodo.task.TaskList
import mytodo.ui.widgets.UndefinedController

class ListsList(ds : DataSource) extends UserControl(new UndefinedController){

  override protected def bindData(root : Node) = {
    val list = root.asInstanceOf[ListView[TaskList]];
    list.setItems( Util.createObservableList(ds.getLists) );
  }

}