package mytodo.ui.widgets.goals_list


import mytodo.ui.widgets.user_control.UserControl
import mytodo.NotImplemented
import javafx.scene.control.TreeView
import mytodo.task.Goal
import javafx.scene.control.TreeItem
import javafx.collections.ObservableList
import javafx.collections.FXCollections
import mytodo.Util
import mytodo.Main
import javafx.util.Callback
import javafx.scene.Node
import mytodo.task.TaskList
import mytodo.task.Task
import javafx.scene.control.TreeCell
import mytodo.ui.widgets.UndefinedController

class GoalsList extends UserControl(new UndefinedController){

  override protected def bindData(root : Node) = {
    val treeRoot = new TreeItem[TreeNode](RootNode());
    Main.dataSource.getGoals map (addChild(treeRoot, _))
    val tree = root.lookup("#goals_tree").asInstanceOf[TreeView[TreeNode]];
    tree.setRoot(treeRoot);
    tree.setCellFactory(new CellFactory());
    tree.setShowRoot(false);
  }

  def addChild(item : TreeItem[TreeNode], goal : Goal) : Unit = {
    val childItem = new TreeItem[TreeNode](GoalNode(goal));
    item.getChildren().add(childItem);
    goal.children.map(addChild(childItem, _))
    val taskLists = Main.dataSource.getLists filter (_.goal.getID == goal.getID);
    taskLists.map(addChild(childItem, _));
  }
  
  def addChild(item : TreeItem[TreeNode], list : TaskList) : Unit = {
    val childItem = new TreeItem[TreeNode](ListNode(list));
    item.getChildren.add(childItem);
    list.contents.map { taskid => addChild(childItem, Main.dataSource.getTask(taskid)) };
  }
  
  def addChild(item : TreeItem[TreeNode], task : Task) : Unit = {
    item.getChildren().add( new TreeItem(TaskNode(task)) );
  }
}

private abstract class TreeNode;
private case class RootNode extends TreeNode;
private case class GoalNode(goal : Goal)     extends TreeNode;
private case class ListNode(list : TaskList) extends TreeNode;
private case class TaskNode(task : Task)     extends TreeNode;

private class GoalsTreeCell extends TreeCell[TreeNode]{
  override def updateItem(item : TreeNode, empty : Boolean) : Unit = {
	super.updateItem(item, empty);
	if(empty){
	  setText(null);
	  setGraphic(null);
	}
	else{
	  item match{
	    case GoalNode(goal) => setText("Goal : " + goal.name);
	    case ListNode(list) => setText("List : " + list.name);
	    case TaskNode(task) => setText("Task : " + task.name);
	    case RootNode()     => setText("Root");
	  }
	}
  }
}

private class CellFactory extends Callback[TreeView[TreeNode], TreeCell[TreeNode]]{
  def call(param : TreeView[TreeNode]) : TreeCell[TreeNode] = return new GoalsTreeCell();
}