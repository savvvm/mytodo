package mytodo.ui.widgets.dialog_view

import javafx.event.ActionEvent
import javafx.fxml.FXML
import mytodo.ui.Controller
import mytodo.ui.DialogController

abstract class DialogViewController extends DialogController{

  var cleanUserInput = true;
  def cleanup;
  def saveData;
  def cleanData;
  
  override def cansel(event : ActionEvent){
    cleanup;
    cleanData;
  }
  
  override def save(event : ActionEvent){
    saveData;
    cleanData;
    if(cleanUserInput){
    	cleanup;
    }
    else{
      cleanUserInput = true;
    }
  }
  
  def canselCleanUserInput = cleanUserInput = false;

}