package mytodo.ui.widgets.dialog_view

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl

abstract class DialogView(controller : DialogViewController) extends UserControl(controller);