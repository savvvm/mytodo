package mytodo.ui.widgets.add_task

import mytodo.ui.widgets.user_control.UserControl
import mytodo.datasource.DataSource
import mytodo.task.Task
import javafx.stage.Stage
import javafx.scene.Node
import mytodo.ui.widgets.dialog_view.DialogView
import javafx.scene.layout.VBox
import javafx.scene.control.CheckBox
import javafx.event.EventHandler
import javafx.event.ActionEvent
import javafx.scene.layout.HBox
import mytodo.task.Context

class AddTask(ds : DataSource, parent : Stage) extends DialogView(new AddTaskController(ds, parent)){
  override protected def bindData(root : Node) = {
    val contexts_list = root.lookup("#contexts_list").asInstanceOf[HBox];
    val contexts = ds.getContexts;
    contexts map { c => contexts_list.getChildren().add(new ContextCheckBox(c))}
  };
  
}

class ContextCheckBox(c : Context) extends CheckBox{
	setText( "@" + c.name);
	//    setOnAction( new EventHandler[ActionEvent]{
	//    	def handle(event : ActionEvent) = {
	//    	  System.err.println("I am here");
	//    	}
	//    });
	def getContext = c;
}
