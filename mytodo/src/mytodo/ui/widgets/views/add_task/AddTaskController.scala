package mytodo.ui.widgets.add_task

import javafx.fxml.FXML
import javafx.event.ActionEvent
import mytodo.datasource.DataSource
import mytodo.datasource.DataError
import mytodo.task._
import javafx.scene.control.TextField
import javafx.scene.control.TextArea
import javafx.stage.Stage
import javafx.scene.control.ListView
import javafx.collections.FXCollections
import mytodo.ui.widgets.dialog_view.DialogViewController
import javafx.scene.control.CheckBox
import javafx.scene.layout.HBox
import mytodo.ui.widgets.restricted_text.DateTimeInput
import mytodo.task.reminder.Reminder
import mytodo.task.reminder.AlarmManager
import mytodo.task.reminder.RepeatFunction
import mytodo.ui.windows.dialogs.reminder_dialog.ReminderSelectorDialog
import mytodo.ui.windows.dialogs.repeat_selector_dialog.RepeatSelectorDialog
import mytodo.ui.windows.dialogs.parent_dialog.ParentSelectDialog
import mytodo.task.reminder.RepeatMethod
import mytodo.task.reminder.RepeatMethod._
import java.util.Calendar

class AddTaskController(ds : DataSource, parent : Stage) extends DialogViewController{

  @FXML
  def showRemSelectWindow(event : ActionEvent) : Unit = {
    val time = start_time_input.getTime;
    val msg = name_input.getText();
    val dialog = new ReminderSelectorDialog(msg, Option(time), parent);
    dialog.showAndWait();
    reminder = dialog.getResult;
  }

  @FXML
  def showRepeatSelectWindow(event : ActionEvent) : Unit = {
    val dialog = new RepeatSelectorDialog(ds, parent);
    dialog.showAndWait();
    dialog.getResult foreach{ case(method, argument)=>
      repeatMethod   = method;
      repeatArgument = argument;
    }
  }


  @FXML
  def addParent(event : ActionEvent) : Unit = {
    val dialog = new ParentSelectDialog(parent, ds);
    dialog.showAndWait();
    dialog.getResult foreach{ parent =>
        parents = parent::parents;
        val items = FXCollections.observableArrayList[TaskList]();
        parents foreach  (items.add _)
        parents_list.setItems(items);
    }
  }
  
  @FXML var name_input          : TextField = null;
  @FXML var is_done_checkbox    : CheckBox  = null;
  @FXML var description_input   : TextArea  = null;
  @FXML var parents_list        : ListView[TaskList] = null;
  @FXML var contexts_list       : HBox = null;

  @FXML var start_time_input    : DateTimeInput = null;
  @FXML var start_time_checkbox : CheckBox = null;
  @FXML var end_time_checkbox   : CheckBox = null;
  @FXML var end_time_input      : DateTimeInput = null;

  @FXML
  def switchStartInput(event : ActionEvent) = swithInputState(start_time_checkbox, start_time_input);

  @FXML
  def switchEndInput(event : ActionEvent) = swithInputState(end_time_checkbox, end_time_input);
  
  private def swithInputState(checkbox : CheckBox, input : TextField) = {
	  input.setDisable(!checkbox.isSelected());
	  if(checkbox.isSelected() == false){
	    input.setText("");
	  }
  }
  
  override def cleanup = {
    name_input.setText("");
    is_done_checkbox.setSelected(false);
    description_input.setText("");
    parents_list.setItems(FXCollections.emptyObservableList());
    start_time_input.setText("");
    start_time_input.setDisable(true);
    start_time_checkbox.setSelected(false);
    end_time_checkbox.setSelected(false);
    end_time_input.setText("");
    end_time_input.setDisable(true);
    contexts_list.getChildren.toArray map { child =>
      child.asInstanceOf[ContextCheckBox].setSelected(false);
    }
  }

  var parents : List[TaskList] = List();
  var reminder : Option[Reminder] = None;
  var repeatMethod : RepeatMethod = RepeatMethod.NotRepeat;
  var repeatArgument : Any = null;

  override def saveData : Unit = {
    try{
    	val result = ds.createTask(name_input.getText());
    	result.setDoneState(is_done_checkbox.isSelected());
    	result.description = description_input.getText();
    	result.startTime   = start_time_input.getTime;
    	result.endTime     = end_time_input.getTime;
      reminder foreach  { rem =>
    	  result.remindMethod = rem;
    	  AlarmManager.registerReminder(rem); //XXX: should i move register in Task class?
    	}
    	result.repeat = createRepeater(result.getID, repeatMethod, repeatArgument);
    	parents foreach (result.addParent(_));
    	parents foreach ( p => assert(p.contents.contains(result.getID)) ); //DEBUG
    	getContexts foreach (result.addContext(_));
    	ds.saveTask(result);
    	//System.out.println(result.debugImage);
    }
    catch {
    	case e : DataError => e.printStackTrace(); //XXX: empty catch block
    }
  }
  
  private def createRepeater(taskID : Long, method : RepeatMethod, argument : Any) : RepeatFunction = method match{
    case RepeatMethod.NotRepeat           => return ds.createNotRepeat(taskID);
    case RepeatMethod.RepeatAtDate        => return ds.createRepeatAtDate(taskID, argument.asInstanceOf[Calendar]);
    case RepeatMethod.DayOfMonth          => return ds.createDayOfMonth(taskID, argument.asInstanceOf[Int]);
    case RepeatMethod.RepeatAfterInterval => return ds.createRepeatAfterInterval(taskID, argument.asInstanceOf[Long]);
  }
  
  private def getContexts : Array[Context] = {
    val contextAndNulls = contexts_list.getChildren.toArray map { child =>
      val chb = child.asInstanceOf[ContextCheckBox];
      if(chb.isSelected()) chb.getContext; else null;
    }
    return contextAndNulls filter (_ != null);
  }

  override def cleanData : Unit = {
    parents  = List();
    reminder = null;
    repeatMethod = null;
    repeatArgument  = null;
  }
}