package mytodo.ui.widgets.task_widget

import javafx.scene.Node
import mytodo.ui.widgets.user_control.UserControl
import javafx.scene.control.Label
import mytodo.task.Task
import javafx.event.ActionEvent
import mytodo.{Util, NotImplemented, Main}
import javafx.fxml.FXML
import mytodo.ui.Controller
import java.util.Calendar
import javafx.scene.layout.{BorderPane, HBox, VBox}
import javafx.scene.text.Text
import mytodo.task.Context
import java.text.SimpleDateFormat
import javafx.util.Callback
import mytodo.ui.widgets.edit_task.EditTask
import javafx.stage.Stage
;

class TaskWidget(task : Task, stage : Stage, parent : BorderPane) extends UserControl(new ControllerFactory(task, stage, parent)){
  getStylesheets().add(getClass.getResource("TaskWidgetStyle.css").toExternalForm());

  @deprecated
  protected def bindData(n: Node): Unit = {}
}

class ControllerFactory(task : Task, stage : Stage, parent : BorderPane) extends Callback[Class[_], AnyRef]{
  def call(p1: Class[_]): AnyRef = return new TaskController(task, stage, parent);
}

class TaskController(task : Task, stage : Stage, parent : BorderPane) extends Controller{

  @FXML val parent_names_output : HBox  = null;
  @FXML val task_name_output    : Label = null;
  @FXML val task_header_layout  : HBox  = null;
  @FXML val description_output  : Label = null;
  @FXML val task_widget_root    : VBox  = null;
  @FXML val contexts_output     : HBox  = null;
  @FXML val start_time_output   : Label = null;
  @FXML val end_time_output     : Label = null;

  @FXML
  def markTaskAsDone(event : ActionEvent) : Unit = {
    throw new NotImplemented();
    //TODO: Not implemented
  }

  @FXML
  def openTaskEditor(event : ActionEvent) : Unit = {
    parent.setCenter(new EditTask(task, Main.dataSource, stage));
  }

  @FXML
  def removeTask(event : ActionEvent) : Unit = {
    throw new NotImplemented();
    //TODO: Not implemented
  }

  private def createParentLabels(parentIDs : List[Long]) : List[Label] = {

    def makeLabel(parentID : Long) : Label = {
      val l = new Label();
      val list = Main.dataSource.getList(parentID);
      l.setText(list.name);
      return l;
    }

    return  parentIDs match{
      case List(x) => List( makeLabel(x) );
      case x::xs   => makeLabel(x)::(new Label(" | "))::createParentLabels(xs);
      case List()  => List();
    }
  }

  private def getTimeClass(t : Task) : String = {
    val now = Calendar.getInstance();
    if(t.startTime.isDefined && t.startTime.get.before(now)){ return "overdue"; } else{ return "normal"; }
  }

  private def calculateTextWidth(s : String) : Double = {
    val text = new Text(s);
    text.snapshot(null, null);
    return text.getLayoutBounds.getWidth();
  }

  private def setTime(output : Label, item : Option[Calendar], default : String) ={
    def chooseFormat(day : Int) : SimpleDateFormat = {
      val today = Calendar.getInstance();
      if(today.get(Calendar.DAY_OF_YEAR) == day){
        return Util.DateFormats.todayFormatter
      }
      else{
        return Util.DateFormats.fullFormatter;
      }
    }

    item match{
      case None => output.setText(default);
      case Some(time) => {
        val format = chooseFormat(time.get(Calendar.DAY_OF_YEAR));
        output.setText(format.format(time.getTime()));
      }
    }
  }

  private def makeContextLabel(context : Context) : Label = {
    val name = new Label("@" + context.name + " ");
    name.getStyleClass().add( "context_label_class" );
    return name;
  }

  def initialize(){
    val startLabel = List( new Label("[") );
    val endLabel   = List( new Label("]") );
    val parentLabels = createParentLabels(task.parents );
    if(parentLabels.length > 0){
      (startLabel ++ parentLabels ++ endLabel) foreach (label => parent_names_output.getChildren.add(label));
    }
    task_name_output.setText(task.name);
    task_header_layout.getStyleClass().add( getTimeClass(task) );
    if(task.description.isEmpty){
      task_widget_root.asInstanceOf[VBox].getChildren.remove(description_output);
    }
    else{
      description_output.setText(task.description);
    }
    val contextLabels = task.contexts map makeContextLabel;
    contextLabels map contexts_output.getChildren().add
    setTime(start_time_output, task.startTime, "[No start time]");
    start_time_output.setMinWidth( calculateTextWidth(start_time_output.getText()) );
    setTime(end_time_output, task.startTime, "[No end time]");
    end_time_output.setMinWidth( calculateTextWidth(end_time_output.getText()) );
  }
}