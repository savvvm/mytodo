package mytodo.ui.windows.main_window

import javafx.fxml.FXML
import javafx.event.ActionEvent
import mytodo.NotImplemented
import javafx.scene.layout.BorderPane
import mytodo.Main
import javafx.stage.Stage
import mytodo.ui.widgets.add_task.AddTask
import mytodo.ui.widgets.task_list.TaskList
import mytodo.ui.widgets.today_view.TodayView
import mytodo.ui.widgets.goals_list.GoalsList
import mytodo.ui.widgets.lists_list.ListsList
import mytodo.ui.widgets.contexts_list.ContextsList
import mytodo.ui.widgets.calendar_view.CalendarView
import mytodo.ui.widgets.inbox_view.InboxView
import javafx.scene.control.TextField
import mytodo.ui.widgets.add_alarm.AddAlarm
import javafx.scene.control.TextArea
import mytodo.ui.widgets.add_list.AddList
import mytodo.ui.widgets.add_goal.AddGoal
import mytodo.ui.widgets.add_context.AddContext

class Controller(s : Stage){

  @FXML var main_window_pane : BorderPane = null;
  
  @FXML
  def createAlarm(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new AddAlarm );
  }
  
  @FXML
  def createTask(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new AddTask(Main.dataSource, s) );
  }

  @FXML
  def createList(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new AddList(Main.dataSource, s) );
  }

  @FXML
  def createGoal(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new AddGoal(Main.dataSource) );
  }

  @FXML
  def createContext(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new AddContext(Main.dataSource) );
  }

  @FXML
  def showToday(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new TodayView(Main.dataSource, s, main_window_pane) );
  }

  @FXML
  def showAll(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new TaskList(main_window_pane, s) );
  }

  @FXML
  def showGoals(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new GoalsList );
  }

  @FXML
  def showLists(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new ListsList(Main.dataSource) );
  }

  @FXML
  def showContexts(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new ContextsList(Main.dataSource) );
  }

  @FXML
  def showCalendar(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new CalendarView );
  }

  @FXML
  def showInbox(event : ActionEvent) : Unit = {
    main_window_pane.setCenter( new InboxView );
  }

  @FXML var inbox_input : TextArea = null;

  @FXML
  def addToInbox(event : ActionEvent) : Unit = {
    if(inbox_input != null && inbox_input.getText() != ""){
      Main.dataSource.putIntoInbox(inbox_input.getText());
      inbox_input.setText("");
    }
  }
  
  @FXML
  def addToTasks(event : ActionEvent) : Unit = {
    throw new NotImplemented();
    //TODO: Not implemented
  }

}