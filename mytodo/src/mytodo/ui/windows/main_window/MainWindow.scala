package mytodo.ui.windows.main_window

import javafx.stage.Stage
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.fxml.FXMLLoader
import java.net.URI
import javafx.scene.layout.BorderPane
import mytodo.ui.widgets.task_list.TaskList
import mytodo.ui.widgets.today_view.TodayView
import mytodo.Main

class MainWindow(stage : Stage) {
  
  final val DEFAULT_WIDTH  = 800;
  final val DEFAULT_HEIGHT = 700;

  val url = getClass().getResource("MainWindowLayout.fxml");
  assert(url != null);
  val loader = new FXMLLoader(url);
  loader.setController(new Controller(stage))
  val root : Parent = loader.load().asInstanceOf[Parent];
  root.asInstanceOf[BorderPane].setCenter(new TodayView(Main.dataSource, stage, root.asInstanceOf[BorderPane]));
  val scene = new Scene(root);

  def open = {
    stage.setTitle("Roadmap todo & GTD application");
    stage.setScene(scene);
    stage.show();
    stage.setWidth(DEFAULT_WIDTH);
    stage.setHeight(DEFAULT_HEIGHT);
  }

}