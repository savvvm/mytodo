package mytodo.ui.windows.reminder

import mytodo.ui.LayoutLoader
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.fxml.FXML
import javafx.event.ActionEvent
import javafx.scene.Node
import javafx.stage.Stage
import javafx.scene.Group

class MessageRemindWindow(messageText : String) extends Stage with LayoutLoader{


  def bindData(root : Node){
    val label = root.lookup("#message_output").asInstanceOf[Label];
    label.setText(messageText);
  }

  var delayNeeded : Boolean = false;
  val parent = loadLayout(new Controller(this));
  setScene(new Scene(new Group(parent)));
  
}

private class Controller(parent : MessageRemindWindow) extends mytodo.ui.Controller{
  
  @FXML
  def close(event : ActionEvent){
    parent.close();
  }
  
  @FXML
  def delay(event : ActionEvent){
    parent.delayNeeded = true;
    parent.close();
  }
  
}