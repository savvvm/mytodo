package mytodo.ui.windows.dialogs.parent_dialog

import javafx.stage.Stage
import mytodo.task.TaskList
import mytodo.datasource.DataSource
import mytodo.ui.windows.dialogs.list_selection_dialog.ListSelectionDialog

class ParentSelectDialog(owner : Stage, dataSource : DataSource) extends ListSelectionDialog[TaskList](owner, dataSource.getLists){

  var result : Option[TaskList] = None;

}