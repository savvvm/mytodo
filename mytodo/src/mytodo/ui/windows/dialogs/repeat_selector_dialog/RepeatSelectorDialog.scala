package mytodo.ui.windows.dialogs.repeat_selector_dialog

import javafx.scene.Node
import mytodo.ui.windows.dialogs.Dialog
import mytodo.datasource.DataSource
import javafx.stage.Stage
import mytodo.task.reminder.RepeatFunction
import javafx.scene.control.ComboBox
import mytodo.Util
import mytodo.task.reminder.RepeatFunction
import mytodo.task.reminder.NotRepeat
import mytodo.task.reminder.RepeatMethod
import mytodo.task.reminder.RepeatMethod._
import javafx.fxml.FXML
import javafx.event.ActionEvent
import mytodo.NotImplemented
import mytodo.ui.widgets.restricted_text.DateTimeInput
import mytodo.ui.widgets.restricted_text.RestrictedText
import mytodo.ui.widgets.restricted_text.RestrictedText
import mytodo.ui.widgets.restricted_text.DayInput
import mytodo.task.reminder.RepeatMethod
import mytodo.task.reminder.RepeatAtdate
import mytodo.task.reminder.DayOfMonth
import java.util.Calendar
import mytodo.task.reminder.RepeatAfterInterval
import mytodo.ui.windows.dialogs.DialogWindowController
import mytodo.task.reminder.RepeatFunction
import javafx.scene.layout.HBox

class RepeatSelectorDialog(ds : DataSource, parent : Stage) extends Dialog[(RepeatMethod, Any), (RepeatMethod, Any)](parent){

  private var result : Option[(RepeatMethod, Any)] = None;
  
  def bindData(root : Node): Unit = {
    val repeat_function_selector = root.lookup("#repeat_function_selector").asInstanceOf[ComboBox[RepeatMethod]];
    repeat_function_selector.setItems(Util.createObservableList(RepeatMethod.values.toList));
  }
  
  def getResult = result;
  override def getControllerInstance() : DialogWindowController[(RepeatMethod, Any)] = return new Controller(saveFunction, this);
  
  private def saveFunction(rep : Option[(RepeatMethod, Any)]) = result = rep;
}

class Controller(retfun : (Option[(RepeatMethod, Any)] => Unit), stage : Stage) extends DialogWindowController[(RepeatMethod, Any)](stage, retfun){

  @FXML var repeat_function_selector : ComboBox[RepeatMethod]   = null;
  @FXML var input_box : HBox = null;
  
  @FXML def repeat_metod_changed(event : ActionEvent) : Unit = {
    repeat_function_selector.getSelectionModel().getSelectedItem() match{
      case RepeatMethod.NotRepeat           => removeContent;
      case RepeatMethod.RepeatAtDate        => removeContent; showDateTimeInput;
      case RepeatMethod.DayOfMonth          => removeContent; showDayInput;
      case RepeatMethod.RepeatAfterInterval => removeContent; showDateTimeInput;
    }
  }

  def showDateTimeInput = input_box.getChildren.add( new DateTimeInput  );
  def showDayInput      = input_box.getChildren.add( new DayInput );
  def removeContent     = if(input_box.getChildren.size() > 0){
      for(i <- 0 to input_box.getChildren.size() - 1){
        input_box.getChildren.remove(input_box.getChildren.get(i));
      }
  }

  def getContent[Type] = input_box.getChildren().asInstanceOf[Type];


  override def createResult : (RepeatMethod, Any)= {
     val method = repeat_function_selector.getSelectionModel().getSelectedItem();
     var argument : Any = null;
     method match{
       case RepeatMethod.NotRepeat           => return (method, null);
       case RepeatMethod.RepeatAtDate        => argument = getContent[DateTimeInput].getTime;
       case RepeatMethod.DayOfMonth          => argument = getContent[DayInput].getDay;
       case RepeatMethod.RepeatAfterInterval =>{
    	   val dt  = getContent[DateTimeInput].getTime;
    	   val cal = Calendar.getInstance();
    	   argument = dt.getTimeInMillis() - cal.getTimeInMillis();
       } 
    }
    return (method, argument)
  }
}