package mytodo.ui.windows.dialogs.child_dialog

import mytodo.task.Task
import javafx.stage.Stage
import mytodo.datasource.DataSource
import mytodo.ui.windows.dialogs.list_selection_dialog.ListSelectionDialog

class ChildSelectDialog(owner : Stage, dataSource : DataSource) extends ListSelectionDialog[Task](owner, dataSource.getTasks){ //TESTME
  var result : Option[Task] = None;
}