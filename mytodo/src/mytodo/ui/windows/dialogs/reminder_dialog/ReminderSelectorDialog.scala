package mytodo.ui.windows.dialogs.reminder_dialog

import mytodo.ui.windows.dialogs.Dialog
import javafx.stage.Stage
import mytodo.task.reminder.Reminder
import mytodo.task.reminder.RemindMode
import mytodo.task.reminder.RemindMode._
import javafx.scene.control.ComboBox
import mytodo.Util
import javafx.fxml.FXML
import mytodo.task.reminder.MessageReminder
import javafx.scene.control.Button
import java.util.Calendar
import javafx.scene.Node
import javafx.scene.control.Label
import mytodo.ui.windows.dialogs.DialogWindowController

class ReminderSelectorDialog(msg : String, time : Option[Calendar], parent : Stage) extends Dialog[Reminder, RemindMode](parent){

  var result : Option[Reminder] = None;
  override def getResult : Option[Reminder] = result;

  def bindData(root : Node): Unit = {
    val methods = RemindMode.values;
    val remind_method_selector = root.lookup("#remind_method_selector").asInstanceOf[ComboBox[RemindMode]];
    remind_method_selector.setItems(Util.createObservableList(methods.toList));
    disableSaveIf(root, msg == null || msg.isEmpty(), "Message in task window is not set");  //TESTME
    disableSaveIf(root, !time.isDefined, "Start time in task window is not set");  //TESTME
  }
  
  def disableSaveIf(root : Node, cond : Boolean, msg : String){
	val save_button          = root.lookup("#save_button").asInstanceOf[Button];
	val error_message_output = root.lookup("#error_message_output ").asInstanceOf[Label];
    if(cond){
      save_button.setDisable(true);
      error_message_output.setVisible(true);
      error_message_output.setText(msg);
    }
  }

  override def getControllerInstance() : DialogWindowController[RemindMode] = {
    return new Controller(returnFunction, this);
  }

  def returnFunction(mode : Option[RemindMode]) = {
    if(time.isDefined){
      mode match{
        case Some(Message) => result = Some(new MessageReminder(time.get, msg));
        case _             => result = None;
      }
    }
  }
}

class Controller(retFun: (Option[RemindMode] => Unit), s: Stage) extends DialogWindowController[RemindMode](s, retFun){
  @FXML var remind_method_selector : ComboBox[RemindMode] = null;
  
  def createResult : RemindMode = {
    return remind_method_selector.getSelectionModel().getSelectedItem();
  }

}