package mytodo.ui.windows.dialogs

import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.Modality
import mytodo.ui.LayoutLoader
import javafx.scene.Scene
import javafx.scene.Node
import mytodo.ui.Controller
import javafx.scene.Group

abstract class Dialog[Type, ControllerType](owner : Stage) extends Stage with LayoutLoader{

    def getControllerInstance : DialogWindowController[ControllerType];
    def bindData(n : Node) : Unit;
    def getResult : Option[Type];

    val parent = loadLayout(getControllerInstance);
    setScene(new Scene(new Group(parent)));
    initOwner(owner);
    initStyle(StageStyle.UTILITY);
    initModality(Modality.WINDOW_MODAL);

}