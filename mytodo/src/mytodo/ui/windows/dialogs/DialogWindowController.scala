package mytodo.ui.windows.dialogs

import javafx.event.ActionEvent
import javafx.fxml.FXML
import mytodo.ui.Controller
import mytodo.ui.DialogController
import javafx.stage.Stage

abstract class DialogWindowController[ResultType](stage : Stage, saveData : (Option[ResultType] => Unit)) extends DialogController{

  def createResult : ResultType;
  
  @FXML
  override def cansel(event : ActionEvent){
    stage.close();
  }
  
  @FXML
  override def save(event : ActionEvent){
    saveData(Some(createResult));
    stage.close();
  }

}