package mytodo.ui.windows.dialogs.goal_dialog

import javafx.stage.Stage
import mytodo.datasource.DataSource
import mytodo.task.Goal
import mytodo.ui.windows.dialogs.list_selection_dialog.ListSelectionDialog

class GoalSelectDialog(owner : Stage, dataSource : DataSource) extends ListSelectionDialog[Goal](owner, dataSource.getGoals){ //TESTME
  var result : Option[Goal] = None;
}