package mytodo.ui.windows.dialogs.list_selection_dialog

import mytodo.ui.windows.dialogs.Dialog
import javafx.stage.Stage
import mytodo.ui.windows.dialogs.Dialog
import javafx.scene.control.ListView
import mytodo.Util
import javafx.scene.control.SelectionMode
import java.net.URL
import java.net.URI
import javafx.scene.Node
import mytodo.task.reminder.RepeatFunction
import mytodo.ui.windows.dialogs.DialogWindowController

abstract class ListSelectionDialog[T](owner : Stage, elements : List[T]) extends Dialog[T, T](owner){

  var result : Option[T];

  override def bindData(root : Node) : Unit = {
    val list = root.lookup("#list").asInstanceOf[ListView[T]];
    list.setItems(Util.createObservableList(elements));
    list.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
  }
 
  def getResult = result;

  override def getControllerInstance() : DialogWindowController[T] = {
    val cont = new ListSelectorController[T](saveSelection, this);
    return cont;
  }
  def saveSelection(item : Option[T]) = result = item;

}