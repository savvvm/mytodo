package mytodo.ui.windows.dialogs.list_selection_dialog

import javafx.event.ActionEvent
import javafx.scene.control.ListView
import mytodo.task.TaskList
import javafx.fxml.FXML
import mytodo.task.TaskList
import javafx.stage.Stage
import mytodo.ui.windows.dialogs.DialogWindowController

class ListSelectorController[T](retFun : (Option[T] => Unit), stage : Stage) extends DialogWindowController[T](stage, retFun){

  @FXML var list : ListView[T] = null;
  
  def createResult : T = return  list.getSelectionModel().getSelectedItems().get(0);


}