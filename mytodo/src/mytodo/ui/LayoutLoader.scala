package mytodo.ui
import java.net.URL
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import java.io.IOException
import javafx.util.Callback

trait LayoutLoader{

    protected def getViewURL : URL = {
      val layoutPathTemplate = "%sLayout.fxml";
      val path = String.format(layoutPathTemplate, getClass.getSimpleName);
      return getClass.getResource(path);
    }

    @deprecated
    protected def bindData(n : Node);
    
    @throws[IOException]
    @deprecated
    def loadLayout(controller : Controller) : Node = {
      val loader = new FXMLLoader;
      loader.setLocation(getViewURL);
      loader.setController(controller);
      val layout : Node = loader.load().asInstanceOf[Node];
      bindData(layout);
      return layout;
    }

  @throws[IOException]
  def loadLayout(factory : Callback[Class[_], AnyRef]) : Node = {
    val loader = new FXMLLoader;
    loader.setLocation(getViewURL);
    loader.setControllerFactory(factory);
    val layout : Node = loader.load().asInstanceOf[Node];
    bindData(layout);
    return layout;
  }


}