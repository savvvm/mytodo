package mytodo.ui

import javafx.event.ActionEvent
import javafx.fxml.FXML

trait DialogController extends Controller {

  @FXML
  def save(event : ActionEvent) : Unit;
  
  @FXML
  def cansel(event : ActionEvent) : Unit;
}